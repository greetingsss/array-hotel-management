package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Order {

  private int id;
  private String orderNum;
  private String orderStatus;
  private String orderYdTime;
  private String orderRzTime;
  private int orderDay;
  private int userId;
  private int kfId;
  private double orderPayMoney;
  private String orderPayStatus;
  private String evaPing;

  private String orderMonth;//月份
  private double money;//总金额
  private User user;
  private TbRoom tbRoom;

}

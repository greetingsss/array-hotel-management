package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Eva {

  private int id;
  private String evaContent;
  private String evaTime;
  private int userId;
  private String isNm;
  private int kfId;
  private String appStatus;
  private String evaPing;  //评论
  private String evaMen; //热门

  private User user;
  private KfType kfType;
  private List<EvaReply> evaReply;//回评
  private Evaimg evaimg;//图片


}

package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private int id; //主键
    private String username; //用户名
    private String password; //密码
    private String nickname; //昵称
    private String realname; //真实姓名
    private String gender;//性别
    private String imageUrl; //头像
    private String email;  //邮箱地址
    private String phone; //电话号码
    private String idCard; //身份证号
    private String status; //状态 1.启用 2.禁用 0,删除
    private String createTime; //创建时间
    private int opratorId;  //操作人id
}

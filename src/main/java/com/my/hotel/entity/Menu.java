package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Menu {
  private int id;
  private String menuName; //菜单名称
  private String menuUrl;  //菜单地址
  private String menuPath; //菜单路径
  private String menuPro;  //菜单优先级
  private String menuDesc; //菜单描述
  private String menuIcon; //菜单图标
  private int menuPid;   //父菜单id

  private Menu pmenu;

  private List<Menu> clist;


}

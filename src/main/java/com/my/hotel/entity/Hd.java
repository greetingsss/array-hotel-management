package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Hd {

  private int id;
  private String hdDesc;
  private String hdType;
  private double hdYh;
  private String hdStartTime;
  private String hdEndTime;


}

package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Evahun {
    //点赞数量
    private int id;
    private int userId;
    private int kfId;
    private int tbCoun;//点赞数量
}

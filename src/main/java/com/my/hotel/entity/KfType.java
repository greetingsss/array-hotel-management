package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class KfType {

  private int id;
  private String typeName; //类别名称
  private int typeShu; //入住人数
  private int typeChuang;//床类型

}

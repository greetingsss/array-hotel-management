package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class KfRoom {

  private int id;
  private double kfArea;
  private String kfDesc;
  private double kfPrice;
  private double kfHdPrice;
  private int kfTypeId;
  private int hdId;

  private KfType kfType;
  private KfImg kfImg;
  private TbRoom tbRoom;


}

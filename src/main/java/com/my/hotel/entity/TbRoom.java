package com.my.hotel.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TbRoom {

  private int id;
  private String roomNum;
  private int kfId;
  private int roomStatus;



}

package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EvaReply {

  private int id;
  private int evaId;
  private String replyContent;
  private String replyTime;
  private int kfId;//客房id


}

package com.my.hotel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Log {

  private int id;
  private String optTime;
  private String optContent;
  private int optId;

  private User user;

}

package com.my.hotel.mapper;

import com.my.hotel.entity.Role;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper {

    //查询所有角色
    @Select("select * from tb_role")
    public List<Role> getList();

    //根据id查询角色对象信息
    @Select("select * from tb_role where id=#{id}")
    public Role getRoleById(int id);

    //修改角色信息
    @ILog("修改角色")
    @Update("update tb_role set role_name=#{roleName},role_desc=#{roleDesc} where id=#{id}")
    public void updRole(Role role);

    //增加角色信息
    @ILog("添加角色")
    @Insert("insert into tb_role values(null,#{roleName},#{roleDesc})")
    public void addRole(Role role);

    //删除角色信息
    @ILog("删除角色")
    @Delete("delete from tb_role where id=#{id}")
    public void delRole(int id);

    //查询用户已分配的角色信息
    @Select("select r.* from tb_user u,tb_role r,tb_user_role ur where u.id=ur.user_id and r.id =ur.role_id and u.id=#{userId}")
    public List<Role> getDistRole(int userId);

    //查询用户未分配的角色信息
    @Select("select * from tb_role where id not in(select r.id from tb_user u,tb_role r,tb_user_role ur where u.id=ur.user_id and r.id =ur.role_id and u.id=#{userId})")
    public List<Role> getUnDistRole(int userId);

     //查询客户id
    @Select("select * from tb_role where role_name='客户'")
    public Role getone();

}

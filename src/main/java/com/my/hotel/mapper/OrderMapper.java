package com.my.hotel.mapper;

import com.my.hotel.entity.Order;
import com.my.hotel.entity.TbRoom;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface OrderMapper {
    //查询完成订单
    @Select("select SUM(order_pay_money) as money,order_month from tb_order where order_pay_status=1  group by order_month")
    public List<Order> getList();

    //根据order_status查询订单状态
    @Select("select * from tb_order where order_status=#{orderStatus}")
    public List<Order> getstatus(int orderStatus);
    //确认订单
    @Update("update tb_order set order_status=2 where id=#{id}")
    public void ordupdete(int id);
    @Update("update tb_room set room_status=2 where kf_id=#{kfId}")
    public void roomupdate(int kfId);
    @Select("select * from tb_room where kf_id=#{kfId}")
    public TbRoom roomone(int kfId);
    @Select("select * from tb_order where user_id=#{userId}")
    public List<Order> orderList(int userId);
    //添加订单
    @Insert("insert into tb_order values(null,#{orderNum},#{orderStatus},#{orderYdTime},#{orderRzTime},#{orderDay}," +
            "#{userId},#{kfId},#{orderPayMoney},#{orderPayStatus},#{orderMonth},0)")
    public void orderadd(Order order);
    @Select("select MAX(order_num) from tb_order")
    public int selone();
    //修改房间状态
    @Update("update  tb_room set room_status=1 where kf_id=#{kfId}")
    public void updateroom(int kfId);
    //预定
    @Select("select * from tb_order where order_status=0 and user_id=#{userId}")
    public List<Order> getyuding(int userId);
    //入住
    @Select("select * from tb_order where order_status=2 and user_id=#{userId}")
    public List<Order> getrzhu(int userId);
    //退房
    @Select("select * from tb_order where order_status=3 and eva_ping=0 and user_id=#{userId}")
    public List<Order> gettui(int userId);

    //改变是否评论
    @Update("update tb_order set eva_ping=1 where id=#{id}")
    public void upping(int id);

    //确认完成订单
    @Update("update tb_order set order_status=3 where id=#{id}")
    public void ordwang(int id);
    @Update("update tb_room set room_status=0 where kf_id=#{kfId}")
    public void roomwang(int kfId);

}

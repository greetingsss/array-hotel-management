package com.my.hotel.mapper;

import com.my.hotel.entity.KfImg;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface KfImgMapper {

    @Insert("insert into tb_kf_img values(null,#{kfId},#{kfImgPath})")
    public void addKfImg(KfImg kfImg);

    //查询房间对应的图片
    @Select("select * from tb_kf_img where kf_id=#{kfId}")
    public KfImg getimg(int kfId);
}

package com.my.hotel.mapper;

import com.my.hotel.entity.EvaReply;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvareplyMapper {
    //添加回评
    @ILog("添加回评")
    @Insert("insert into tb_eva_reply values(null,#{evaId},#{replyContent},#{replyTime},#{kfId})")
    public void addreply(EvaReply evaReply);
    //查询房间回评
    @Select("select * from tb_eva_reply where kf_id=#{kfId}")
    public List<EvaReply> getList(int kfId);
}

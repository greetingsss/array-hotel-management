package com.my.hotel.mapper;

import com.my.hotel.entity.Eva;
import com.my.hotel.entity.Evahun;
import com.my.hotel.entity.Evaimg;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvaMapper {
    //查询
    @Select("select * from tb_eva")
    public List<Eva> getList();
    //删除
    @ILog("删除评价")
    @Delete("delete from tb_eva where id=#{id}")
    public void deleteEva(int id);

    //总数量
    @Select("select count(1) from tb_eva ")
    public int EvaCount();
    //时间排序
    @Select("select * from tb_eva  order by eva_time desc")
    public List<Eva> getshijian();
    //好评排序
    @Select("select * from tb_eva  where eva_ping=5")
    public List<Eva> gethaoping();
    //中评
    @Select("select * from tb_eva  where eva_ping in(3,4)")
    public List<Eva> getzhongping();
    //差评
    @Select("select * from tb_eva  where eva_ping in(1,2)")
    public List<Eva> getchaping();

    //热门排序
    @Select("select * from tb_eva order by eva_men desc")
    public List<Eva> getmen();

    //查询用户对某个房间的点赞
 //   @Select("select * from tb_eva where user_id=#{userId} and kf_id=#{kfId}")
    @Select("select * from tb_eva_hun where user_id=#{userId} and kf_id=#{kfId}")
    public Evahun getdian(@Param("userId") int userId, @Param("kfId") int kfId);
    //添加热门表数
    @Insert("insert into tb_eva_hun values(null,#{userId},#{kfId},1)")
    public void addshu(@Param("userId") int userId, @Param("kfId") int kfId);
    //删除热门表的数据
    @Delete("delete from tb_eva_hun where user_id=#{userId} and kf_id=#{kfId}")
    public void hunupdate(@Param("userId") int userId, @Param("kfId") int kfId);
    //查询房间的点赞数
    @Select("select eva_men from tb_eva where kf_id=#{kfId}")
    public int getshu(int kfId);
    //修改点赞数量
    @Update("update tb_eva set eva_men=#{evaMen} where kf_id=#{kfId}")
    public void kfupdate(Eva eva);

    //添加评论
    @Insert("insert into tb_eva values(null,#{evaContent},#{evaTime},#{userId},#{isNm},#{kfId},#{appStatus}," +
            "#{evaPing},#{evaMen})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void addEva(Eva eva);
    //添加评论图片
    @Insert("insert into tb_eva_img values(null,#{evaId},#{evaImgPath})")
    public void addevaimg(Evaimg evaimg);

    //查询图片
    @Select("select * from tb_eva_img where eva_id=#{evaId}")
    public Evaimg aeleva(int evaId);



}

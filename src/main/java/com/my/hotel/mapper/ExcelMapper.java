package com.my.hotel.mapper;

import com.my.hotel.entity.ExcelDTO;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExcelMapper {

    @Select("select t.column_comment,t.column_name from " +
            "INFORMATION_SCHEMA.COLUMNS t where t.table_name =#{tableName}")
    public List<ExcelDTO> getList(String tableName);
}

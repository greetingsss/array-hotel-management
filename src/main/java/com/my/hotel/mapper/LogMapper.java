package com.my.hotel.mapper;

import com.my.hotel.entity.Log;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogMapper {
     //日志添加
    @Insert("insert into tb_log values(null,#{optTime},#{optContent},#{optId})")
    public void addLog(Log log);
    //查询日志
    @Select("select * from tb_log")
    public List<Log> getPage();
}

package com.my.hotel.mapper;

import com.my.hotel.entity.User;
import com.my.hotel.entity.UserRole;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface UserMapper {

    //查询所有用户信息
    @Select("select * from tb_user")
    public List<User> getList();
    //新增用户信息
   // @ILog("添加用户")
    @Insert("insert into tb_user values(null,#{username},#{password},#{nickname},#{realname},#{gender}," +
            "#{imageUrl},#{email},#{phone},#{idCard},#{status},#{createTime},#{opratorId})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void addUser(User user);

    @Insert("<script> " +
            "insert into tb_user values " +
            "<foreach collection='list' item='user' separator=','>" +
            "(null,#{user.username},#{user.password},#{user.nickname},#{user.realname},#{user.gender}," +
            "null,#{user.email},#{user.phone},#{user.idCard},#{user.status},#{user.createTime},null)" +
            " </foreach>" +
            " </script>")
    public void addBatchUser(List<User> list);
    //删除用户信息;
    @ILog("删除用户信息")
    @Update("update tb_user set status=0 where id=#{id}")
    public void delUser(int id);
    //根据id获取用户信息
    @Select("select * from tb_user where id=#{id}")
    public User getUserById(int id);
    //修改用户信息
    @ILog("修改用户信息")
    @Update("update tb_user set username=#{username},password=#{password},nickname=#{nickname},realname=#{realname}," +
            "gender=#{gender},image_url=#{imageUrl},email=#{email},phone=#{phone},id_card=#{idCard},status=#{status}" +
            " where id=#{id}")
    public void updateUser(User user);

    //通过用户名查询用户信息
    @Select("select * from tb_user where username=#{username}")
    public User getUserByUserName(String username);
   //查询男女个数
    @Select("select gender,COUNT(1) as count from tb_user group by gender")
    public List<Map> getGender();
    //页面个人信息修改
    @Update("update tb_user set username=#{username},nickname=#{nickname},realname=#{realname}," +
            "gender=#{gender},image_url=#{imageUrl},email=#{email},phone=#{phone},id_card=#{idCard}" +
            " where id=#{id}")
    public void updateUseryem(User user);

    //修改页面个人密码
    @Update("update tb_user set username=#{username},password=#{password}" +
            " where id=#{id}")
    public void updateUserword(User user);
    //通过 用户名，身份证 找回密码
    @Select("select * from tb_user where username=#{username} and id_card=#{idCard}")
    public User getyong(User user);
    //异步验证用户名唯一
    @Select("select * from tb_user where username=#{username}")
    public User nameone(String username);
     //异步验证身份证号唯一
    @Select("select * from tb_user where id_card=#{idCard}")
    public User cardone(String idCard);

}

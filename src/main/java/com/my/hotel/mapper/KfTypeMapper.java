package com.my.hotel.mapper;

import com.my.hotel.entity.KfType;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KfTypeMapper {

    //查询
    @Select("select * from tb_kf_type")
    public List<KfType> getList();

    //新增
    @ILog("添加客房")
    @Insert("insert into tb_kf_type values(null,#{typeName},#{typeShu},#{typeChuang})")
    public void addKfType(KfType kfType);

    //修改
    @ILog("修改客房")
    @Update("update tb_kf_type set type_name=#{typeName},type_shu=#{typeShu},type_chuang=#{typeChuang} where id=#{id}")
    public void updateKfType(KfType kfType);

    //根据id查询对象信息
    @Select("select * from tb_kf_type where id=#{id}")
    public KfType getKfTypeById(int id);

    //删除
    @ILog("删除客房")
    @Delete("delete from tb_kf_type where id=#{id}")
    public void deleteKfType(int id);


    //模糊查询
    @Select("<script>" +
            "select * from tb_kf_type where 1=1 " +
            "<if test='typeShu != 0'>" +
            " and type_shu=#{typeShu}" +
            "</if>" +
            "<if test='typeChuang!=0'>" +
            " and type_chuang=#{typeChuang}" +
            "</if>" +
            "</script>")
     public List<KfType> getmohu(@Param("typeChuang") int typeChuang, @Param("typeShu") int typeShu);
}

package com.my.hotel.mapper;

import com.my.hotel.entity.Menu;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuMapper {
    //查询所有菜单
    @Select("select * from tb_menu")
    public List<Menu> getList();

    //根据id查询菜单对象信息
    @Select("select * from tb_menu where id=#{id}")
    public Menu getMenuById(int id);

    //修改菜单信息
    @ILog("修改菜单")
    @Update("update tb_menu set menu_name=#{menuName},menu_url=#{menuUrl},menu_path=#{menuPath},menu_pro=#{menuPro}," +
            "menu_desc=#{menuDesc},menu_icon=#{menuIcon},menu_pid=#{menuPid} where id=#{id}")
    public void updMenu(Menu menu);

    //增加菜单信息
    @ILog("添加菜单")
    @Insert("insert into tb_menu values(null,#{menuName},#{menuUrl},#{menuPath},#{menuPro},#{menuDesc},#{menuIcon}," +
            "#{menuPid})")
    public void addMenu(Menu menu);

    //删除菜单信息
    @ILog("删除菜单")
    @Delete("delete from tb_menu where id=#{id}")
    public void delMenu(int id);

    //查询角色分配的菜单
    @Select("select m.* from tb_role r,tb_menu m,tb_role_menu rm where r.id =rm.role_id and m.id = rm.menu_id and r.id=#{roleId}")
    public List<Menu> getDistMenu(int roleId);
    //查询角色未分配的菜单
    @Select("select * from tb_menu where id not in(select m.id from tb_role r,tb_menu m,tb_role_menu rm where r.id =rm.role_id and m.id = rm.menu_id and r.id=#{roleId})")
    public List<Menu> getUnDistMenu(int roleId);

    //根据用户id查询菜单列表
    /*@Select("select m.* from tb_menu m, tb_user u,tb_role r, tb_user_role ur, tb_role_menu rm\n" +
            "where u.id =ur.user_id and r.id =ur.role_id and r.id =rm.role_id and m.id =rm.menu_id\n" +
            "and u.id=#{userId} and m.menu_pid =0")
    public List<Menu> getMenuList(int userId);

    @Select("select * from tb_menu where menu_pid=#{menuPid}")
    public List<Menu> getClist(int menuPid);*/

    @Select("select m.* from tb_menu m, tb_user u,tb_role r, tb_user_role ur, tb_role_menu rm\n" +
            "where u.id =ur.user_id and r.id =ur.role_id and r.id =rm.role_id and m.id =rm.menu_id\n" +
            "and u.id=#{userId} and m.menu_pid =#{menuPid} order by m.menu_pro asc")
    public List<Menu> getMenuList2(@Param("userId") int userId,@Param("menuPid") int menuPid);
}

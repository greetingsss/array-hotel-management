package com.my.hotel.mapper;

import com.my.hotel.entity.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleMapper {

    //删除用户关联的角色数据
    @Delete("delete from tb_user_role where user_id=#{userId}")
    public void delUserRole(int userId);

    @Insert("<script>" +
            "insert into tb_user_role values " +
            "<foreach collection=\"list\" item=\"ur\" separator=\",\"> " +
            "(null,#{ur.roleId},#{ur.userId})"+
            "</foreach>"+
            "</script>")
    public void addUserRole(List<UserRole> list);

    //添加注册用户角色
    @Insert("insert into tb_user_role values(null,#{roleId},#{userId})")
    public void adduser(UserRole userRole);

    //查询用户对应的角色
    @Select("select e.* from tb_user u,tb_user_role e where  e.user_id=u.id and u.id=#{userId}")
    public UserRole getRole( int userId);
}

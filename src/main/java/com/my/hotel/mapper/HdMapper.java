package com.my.hotel.mapper;

import com.my.hotel.entity.Hd;
import com.my.hotel.entity.KfRoom;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HdMapper {
    //查询
    @Select("select * from tb_hd")
    public List<Hd> getList();

    //新增
    @ILog("新增活动")
    @Insert("insert into tb_hd values(null,#{hdDesc},#{hdType},#{hdYh},#{hdStartTime},#{hdEndTime})")
    public void addHd(Hd hd);

    //根据id查询对象信息
    @Select("select * from tb_Hd where id =#{id}")
    public Hd getHdById(int id);

    //修改
    @ILog("修改活动")
    @Update("update tb_Hd set hd_desc=#{hdDesc},hd_type=#{hdType},hd_yh=#{hdYh},hd_start_time=#{hdStartTime}," +
            "hd_end_time=#{hdEndTime} where id=#{id}")
    public void updateHd(Hd hd);

    //删除
    @ILog("删除活动")
    @Delete("delete from tb_hd where id =#{id}")
    public void deleteHd(int id);

    // 根据当前系统时间查询活动列表
    @Select("select * from tb_hd where hd_start_time<=#{now} and hd_end_time>=#{now}")
    public List<Hd> getListByNow(String now);
}

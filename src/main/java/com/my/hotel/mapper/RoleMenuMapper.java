package com.my.hotel.mapper;

import com.my.hotel.entity.RoleMenu;
import com.my.hotel.entity.UserRole;
import com.my.hotel.util.ILog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMenuMapper {

    //删除用户关联的角色数据
    @ILog("删除角色用户关联表")
    @Delete("delete from tb_role_menu where role_id=#{roleId}")
    public void delRoleMenu(int roleId);
    @ILog("导入角色")
    @Insert("<script>" +
            "insert into tb_role_menu values " +
            "<foreach collection=\"list\" item=\"rm\" separator=\",\"> " +
            "(null,#{rm.roleId},#{rm.menuId})"+
            "</foreach>"+
            "</script>")
    public void addRoleMenu(List<RoleMenu> list);
}

package com.my.hotel.mapper;

import com.my.hotel.entity.KfRoom;
import com.my.hotel.entity.TbRoom;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface KfRoomMapper {

    //查询
    @Select("select * from tb_kf_room")
    public List<KfRoom> getList();

    //新增
    @Insert("insert into tb_kf_room values(null,#{kfArea},#{kfDesc},#{kfPrice},#{kfHdPrice},#{kfTypeId},#{hdId})")
    @Options(useGeneratedKeys=true,keyProperty = "id")
    public void addKfRoom(KfRoom kfRoom);

    //根据id查询对象信息
    @Select("select * from tb_kf_room where id =#{id}")
    public KfRoom getKfRoomById(int id);

    //修改
    @Update("update tb_kf_room set kf_area=#{kfArea},kf_desc=#{kfDesc},kf_price=#{kfPrice},kf_hd_price=#{kfHdPrice}," +
            "kf_type_id=#{kfTypeId},hd_id=#{hdId} where id=#{id}")
    public void updateKfRoom(KfRoom kfRoom);

    //删除
    @Delete("delete from tb_kf_room where id =#{id}")
    public void deleteKfRoom(int id);

    //查询客房type对应房间的平均价格
    @Select("select t.type_name name,AVG(r.kf_price) price from tb_kf_room r , tb_kf_type t " +
            "where r.kf_type_id =t.id group by t.type_name")
    public List<Map> getAvg();
    //根据房间类型id查询房间 连表查询空房间
    @Select("select k.* from tb_kf_room k ,tb_room r where  k.kf_type_id=#{kfTypeId} and k.id=r.kf_id and r.room_status=0 ")
    public List <KfRoom> roomList(int kfTypeId);
    //查询空闲房间
    @Select("select * from tb_room where kf_id=#{kfId} and room_status=0")
    public TbRoom getkong(int kfId);
    //用户预定房间
    @Update("update tb_room set room_status=1 where kf_id=#{kfId}")
    public void yuadd(int kfId);

    //管理员查询订单
    @Select("select * from tb_kf_room")
    public List<KfRoom> selectding();
    //管理员查询订单
    @Select("select * from tb_room where kf_id=#{kfId} and room_status=#{roomStatus}")
    public TbRoom selectone(@Param("kfId") int kfId, @Param("roomStatus") int roomStatus);

    //查询最大房间号
    @Select("select IFNULL( MAX(r.room_num),1) from tb_kf_room k,tb_room r where k.id=r.kf_id and k.kf_type_id=#{kfTypeId}")
     public int maxroom(int kfTypeId);

    //添加房间号
    @Insert("insert into tb_room values (null,#{roomNum},#{kfId},#{roomStatus})")
    public void addroom(TbRoom room);

    //删除房间号
    @Delete("delete from tb_room where kf_id=#{kfId}")
    public void delectroom( int kfId);
}

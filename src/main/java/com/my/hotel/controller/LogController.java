package com.my.hotel.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Eva;
import com.my.hotel.entity.Log;
import com.my.hotel.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/log",produces = "text/html;charset=utf-8")
public class LogController {
    @Autowired
    private LogService logService;
    @RequestMapping("/getPage")
    public String getPage(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Log> list = logService.getPage();
        PageInfo<Log> page = new PageInfo<Log>(list);
        page.getPages();
        page.getTotal();
        model.addAttribute("page",page);
        return "logPage";
    }
}

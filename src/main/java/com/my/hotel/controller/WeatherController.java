package com.my.hotel.controller;

import com.my.hotel.util.QueryHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/wether",produces = "text/html;charset=utf-8")
public class WeatherController {

    @RequestMapping("/getWeaher")
    @ResponseBody
    public String getWeather(String code){
        String s = QueryHelper.queryWeather(code);
        return s;
    }

}

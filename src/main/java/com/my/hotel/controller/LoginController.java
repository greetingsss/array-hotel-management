package com.my.hotel.controller;

import com.my.hotel.entity.User;
import com.my.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
    @Autowired
    private UserService userService;
    @RequestMapping("/checkLogin")
    public String checkLogin(HttpSession session){
        //security 认证授权通过之后 就会跳转到这个方法中来
        //从springsecurity中获取用户编号
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
//        //若编号为anonymousUser，则说明未验证或未验证成功 返回登录页面
//        if("anonymousUser".equals(username)){
//            return "login";
//        }
        //查询出用户信息 返回主页
        User user = userService.getUserByUserName(username);
        if (user != null) {
            session.setAttribute("userinfo", user);
            return "forward:/user/jump";
        }else{
            return "login";
        }
    }

    @RequestMapping("/checkLogin1")
    public String checkLogin1(){
        System.out.println("checkLogin1");
        return null;
    }
    @RequestMapping("/checkLogin2")
    public String checkLogin2(){
        System.out.println("checkLogin2");
        return null;
    }

}

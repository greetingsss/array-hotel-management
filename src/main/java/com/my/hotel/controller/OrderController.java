package com.my.hotel.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.KfRoom;
import com.my.hotel.entity.Menu;
import com.my.hotel.entity.Order;
import com.my.hotel.entity.User;
import com.my.hotel.service.OrderService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "/order",produces = "text/html;charset=utf-8")
public class OrderController {
    @Autowired
    private OrderService orderService;
    //全部订单
    @RequestMapping("/ordPage")
    public String ordPage(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Order> list = orderService.getList();
        PageInfo<Order> page = new PageInfo<Order>(list);
        model.addAttribute("page",page);
        model.addAttribute("sun","1");
        return "forward:/wodedingdan.jsp";
    }
    //根据order_status查询订单状态 预订
    @RequestMapping("/ordtus")
    public String ordtus(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        //订单状态(0,预定 1,取消 2,入住 3,退房
        List<Order> list = orderService.getstatus(0);
        PageInfo<Order> page = new PageInfo<Order>(list);
        model.addAttribute("page",page);

        return "ordPage";
    }
    //根据order_status查询订单状态 入住
    @RequestMapping("/ordzhu")
    public String ordzhu(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        //订单状态(0,预定 1,取消 2,入住 3,退房
        List<Order> list = orderService.getstatus(2);
        PageInfo<Order> page = new PageInfo<Order>(list);
        model.addAttribute("page",page);

        return "ordrzhu";
    }

    //根据order_status查询订单状态 退房
    @RequestMapping("/ordwang")
    public String ordwang(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        //订单状态(0,预定 1,取消 2,入住 3,退房
        List<Order> list = orderService.getstatus(3);
        PageInfo<Order> page = new PageInfo<Order>(list);
        model.addAttribute("page",page);

        return "ordwang";
    }

    //确认订单信息
    @RequestMapping("/ordqr")
    public String ordqr(Integer id,Model model, Integer pageNo,Integer kfId){
     orderService.ordupdete(id,kfId);
     return ordtus( model,  pageNo);
    }

    //确认退房
    @RequestMapping("/qrwang")
    public String qrwang(Integer id,Model model, Integer pageNo,Integer kfId){
      orderService.ordwang(id,kfId);
        return ordzhu( model,  pageNo);
    }


    //用户预定订单
    @RequestMapping("/yuding")
    @ResponseBody
    public String yuding(Order order, HttpSession session){
        User user = (User) session.getAttribute("userinfo");
        order.setUserId(user.getId());
        orderService.orderadd(order);
        return "1";
    }

}

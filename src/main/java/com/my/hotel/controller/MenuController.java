package com.my.hotel.controller;

import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Menu;
import com.my.hotel.entity.Role;
import com.my.hotel.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;
    //分页查询
    @RequestMapping("/menuPage")
    public String getPage(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageInfo<Menu> page = menuService.getPage(pageNo);
        model.addAttribute("page",page);
        return "menuPage";
    }

    //菜单预添加
    @RequestMapping("/addPreMenu")
    public String addPreMenu(Model model){
        List<Menu> list = menuService.getList();
        model.addAttribute("list",list);
        return "menuAdd";
    }
    //菜单添加
    @RequestMapping("/addMenu")
    public String addMenu(Menu menu,Model model){
        menuService.addMenu(menu);
        return getPage(model);
    }

    //菜单删除
    @RequestMapping("/delMenu")
    public String delMenu(int id,Model model){
        menuService.delMenu(id);
        return getPage(model);
    }

    //菜单预修改
    @RequestMapping("/updPreMenu")
    public String updPreMenu(int id,Model model){
        Menu menu = menuService.getMenuById(id);
        model.addAttribute("menu",menu);
        List<Menu> list = menuService.getList();
        for (Menu m : list) {
            if(m.getId()==menu.getId()){
                list.remove(m);
                break;
            }
        }
        model.addAttribute("list",list);
        return "menuUpdate";
    }

    //菜单修改
    @RequestMapping("/updMenu")
    public String updMenu(Menu menu,Model model){
        menuService.updMenu(menu);
        return getPage(model);
    }

    //重载一下
    public String getPage(Model model){
        return getPage(model,1);
    }

}

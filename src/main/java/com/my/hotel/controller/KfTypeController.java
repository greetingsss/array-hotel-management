package com.my.hotel.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.KfType;
import com.my.hotel.entity.User;
import com.my.hotel.service.KfTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/kfType")
public class KfTypeController {

    @Autowired
    private KfTypeService kfTypeService;
    //分页查询
    @RequestMapping("/kfTypePage")
    public String getPage(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<KfType> list = kfTypeService.getList();
        PageInfo<KfType> page = new PageInfo<KfType>(list);

        model.addAttribute("page",page);
        return "kfTypePage";
    }
    //客房类别预添加
    @RequestMapping("/addPreKfType")
    public String addPreKfType(){
        return "kfTypeAdd";
    }
    //客房类别添加
    @RequestMapping("/addKfType")
    public String addKfType(KfType kfType,Model model){
        kfTypeService.addKfType(kfType);
        return getPage(model);
    }
    //客房类别删除
    @RequestMapping("/delKfType")
    public String delKfType(int id,Model model){
        kfTypeService.deleteKfType(id);
        return getPage(model);
    }
    //客房类别预修改
    @RequestMapping("/updPreKfType")
    public String updPreKfType(int id,Model model){
        KfType kfType = kfTypeService.getKfTypeById(id);
        model.addAttribute("kfType",kfType);
        return "kfTypeUpdate";
    }
    //用户修改
    @RequestMapping("/updKfType")
    public String updKfType(KfType kfType,Model model){
        kfTypeService.updateKfType(kfType);
        return getPage(model);
    }




    public String getPage(Model model){
        return getPage(model,1);
    }
}

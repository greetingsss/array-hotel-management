package com.my.hotel.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Hd;
import com.my.hotel.entity.KfRoom;
import com.my.hotel.entity.KfType;
import com.my.hotel.service.HdService;
import com.my.hotel.service.KfRoomService;
import com.my.hotel.service.KfTypeService;
import com.my.hotel.util.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/kfRoom")
public class KfRoomController {

    @Autowired
    private KfRoomService kfRoomService;
    @Autowired
    private KfTypeService kfTypeService;

    @Autowired
    private HdService hdService;

    //分页查询
    @RequestMapping("/kfRoomPage")
    public String getPage(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<KfRoom> list = kfRoomService.getList();
        PageInfo<KfRoom> page = new PageInfo<KfRoom>(list);
        model.addAttribute("page",page);
        return "kfRoomPage";
    }
    //客房信息预添加
    @RequestMapping("/addPrekfRoom")
    public String addPrekfRoom(Model model){
        //查询客房类别信息
        List<KfType> list = kfTypeService.getList();
        model.addAttribute("listType",list);
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String now = sdf.format(date);
        List<Hd> listHd = hdService.getListByNow(now);
        model.addAttribute("listHd",listHd);
        return "kfRoomAdd";
    }
    //客房信息添加
    @RequestMapping("/addkfRoom")
    public String addkfRoom(KfRoom kfRoom,String fileImg,Model model){
        kfRoomService.addKfRoom(kfRoom,fileImg);
        return getPage(model);
    }
    //客房信息删除
    @RequestMapping("/delKfRoom")
    public String delKfRoom(int id,Model model){
        kfRoomService.deleteKfRoom(id);
        return getPage(model);
    }
    //客房信息预修改
    @RequestMapping("/updPreKfRoom")
    public String updPreKfRoom(int id,Model model){
        KfRoom kfRoom = kfRoomService.getKfRoomById(id);
        model.addAttribute("kfRoom",kfRoom);
        //查询客房类别信息
        List<KfType> list = kfTypeService.getList();
        model.addAttribute("listType",list);
        return "kfRoomUpdate";
    }
    //客房信息修改
    @RequestMapping("/updKfRoom")
    public String updKfRoom(KfRoom kfRoom,Model model){
        kfRoomService.updateKfRoom(kfRoom);
        return getPage(model);
    }

    //图片上传
    @RequestMapping("/fileUpload")
    //异步提交
    @ResponseBody
    public String fileUpload(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();
        InputStream is = file.getInputStream();
        Map<String, Object> map = OssUtil.upload(is, filename);
        System.out.println(map);
        String s = JSON.toJSONString(map);
        return s;
    }
    //管理员管理订单  空房间
    @RequestMapping("/roomding")
    public String roomding(Model model, Integer pageNo){

        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<KfRoom> list = kfRoomService.selectding(0);
        ArrayList<KfRoom> rooms = new ArrayList<KfRoom>();
        //清除条件不符合的数据
        for (KfRoom room : list) {
            if(room.getTbRoom()!=null){
                rooms.add(room);
            }
        }

        PageInfo<KfRoom> page = new PageInfo<KfRoom>(rooms);
        model.addAttribute("page",page);
        return "RoomPagegli";
    }
    //管理员管理订单  预定房间
    @RequestMapping("/roomyuding")
    public String roomyuding(Model model, Integer pageNo){

        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<KfRoom> list = kfRoomService.selectding(1);
        ArrayList<KfRoom> rooms = new ArrayList<KfRoom>();
     //清除条件不符合的数据
        for (KfRoom room : list) {
            if(room.getTbRoom()!=null){
              rooms.add(room);
            }
        }
        PageInfo<KfRoom> page = new PageInfo<KfRoom>(rooms);
        model.addAttribute("page",page);
        return "RoomPagegyuding";
    }
    //管理员管理订单  入住房间
    @RequestMapping("/roomwangding")
    public String roomwangding(Model model, Integer pageNo){

        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<KfRoom> list = kfRoomService.selectding(2);

        ArrayList<KfRoom> rooms = new ArrayList<KfRoom>();
        //清除条件不符合的数据
        for (KfRoom room : list) {
            if(room.getTbRoom()!=null){
                rooms.add(room);
            }
        }

        PageInfo<KfRoom> page = new PageInfo<KfRoom>(rooms);
        model.addAttribute("page",page);
        return "RoomPagerzhu";
    }



    //用户预定酒店
    @RequestMapping("/yudingroom")
    public String yudingroom(int kfid,Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<KfRoom> list = kfRoomService.roomList(kfid);
        PageInfo<KfRoom> page = new PageInfo<KfRoom>(list);
        model.addAttribute("page",page);
        return "kfRoomYuding";
    }




    public String getPage(Model model){
        return getPage(model,1);
    }
}

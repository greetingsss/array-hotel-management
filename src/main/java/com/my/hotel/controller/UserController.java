package com.my.hotel.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.*;
import com.my.hotel.service.*;
import com.my.hotel.util.OssUtil;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/user",produces = "text/html;charset=utf-8")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private ExcelService excelService;
    @Autowired
    private EvaService evaService;
    @Autowired
    private KfTypeService kfTypeService;
    @Autowired
    private KfRoomService kfRoomService;
    @Autowired
    private OrderService orderService;

    //主页跳转
    @RequestMapping("/jump")
    public String jump(Model model, HttpSession session){
        //查询用户所有用的菜单权限
        User user =(User) session.getAttribute("userinfo");
        List<Menu> menuList = menuService.getMenuList(user.getId());
        UserRole role = userService.getRole(user.getId());
        if(role.getRoleId()==4){
            PageHelper.startPage(1,10);
            List<Eva> list = evaService.getList();
            PageInfo<Eva> page = new PageInfo<Eva>(list);

            List<KfType> kytype = kfTypeService.getList();
            PageInfo<KfType> pages = new PageInfo<KfType>(kytype);
            model.addAttribute("type",pages);
            model.addAttribute("page",page);
            return "forward:/index.jsp";
        }else {
            model.addAttribute("menuList",menuList);
            model.addAttribute("user",user);
            return "home";
        }

    }
    //分页查询
    @RequestMapping("/userPage")
    public String getPage(Model model,Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<User> list = userService.getList();
        PageInfo<User> page = new PageInfo<User>(list);
        model.addAttribute("page",page);
        return "userPage";
    }
    //用户预添加
    @RequestMapping("/addPreUser")
    public String addPreUser(){
        return "userAdd";
    }
    //用户添加
    @RequestMapping("/addUser")
    public String addUser(User user,Model model){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        user.setCreateTime(sdf.format(date));
        user.setOpratorId(1);
        String encode = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encode);
        userService.addUser(user);
        return getPage(model);
    }
    //用户添加图片
    @RequestMapping("/userfile")
    //异步提交
    @ResponseBody
    public String userfile(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();
        InputStream is = file.getInputStream();
        Map<String, Object> map = OssUtil.upload(is, filename);
        System.out.println(map);
        String s = JSON.toJSONString(map);
        return s;
    }

    //用户删除
    @RequestMapping("/delUser")
    public String delUser(int id,Model model){
        userService.delUser(id);
        return getPage(model);
    }
    //用户预修改
    @RequestMapping("/updPreUser")
    public String updatePreUser(int id,Model model){
        User user = userService.getUserById(id);
        model.addAttribute("user",user);
        return "userUpdate";
    }
    //用户修改
    @RequestMapping("/updUser")
    public String updateUser(User user,Model model){
        String encode = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encode);
        userService.updateUser(user);
        return getPage(model);
    }
    //跳转到分配页面
    @RequestMapping("/toDistRole")
    public String toDistRole(int userId,Model model){
        //查询已分配的角色
        List<Role> distRole = roleService.getDistRole(userId);
        //查询未分配的角色
        List<Role> unDistRole = roleService.getUnDistRole(userId);
        model.addAttribute("distRole",distRole);
        model.addAttribute("unDistRole",unDistRole);
        model.addAttribute("userId",userId);
        return "distRole";
    }

    //添加用户关联的角色
    @RequestMapping("/addUserRole")
    public String addUserRole(int userId,int[] roleId){
        ArrayList<UserRole> list = new ArrayList<UserRole>();
        for (int rid : roleId) {
            UserRole ur = new UserRole();
            ur.setRoleId(rid);
            ur.setUserId(userId);
            list.add(ur);
        }
        userService.addUserRole(list);
        return "success";
    }
    //用户导出
    @RequestMapping("/exportUser")
    public String exportUser(HttpServletResponse response, HttpServletRequest request) throws IOException {
        //把用户数据查询出来
        List<User> list = userService.getList();
        //把数据全部写入到excel对象中 然后把excel的对象通过流的方式写入到本地
        //导入poi的jar
        //获取流对象
        ServletOutputStream os = response.getOutputStream();
        //设置excel文件格式
        response.setContentType("application/vnd.ms-excel");
        //设置打开方式 attachment：附件下载   filename=report.xlsx 文件名为report.xlsx
        response.setHeader("content-Disposition","attachment;filename=exportUser.xlsx");
        //创建excel对象
        XSSFWorkbook sheets = new XSSFWorkbook();
        //创建工作表对象
        XSSFSheet sheet = sheets.createSheet("酒店信息管理系统用户数据列表");
        //添加表头信息 获取表格第一行
        XSSFRow row_title = sheet.createRow(0);
        List<ExcelDTO> lisetExcel = excelService.getList("tb_user");
        for (int i = 0; i < lisetExcel.size(); i++) {
            ExcelDTO excelDTO = lisetExcel.get(i);
            if(excelDTO.getColumnName().equals("oprator_id")){
                continue;
            }
            if(excelDTO.getColumnName().equals("id")){
                row_title.createCell(i).setCellValue("序号");
            }else{
                row_title.createCell(i).setCellValue(excelDTO.getColumnComment());
            }

        }
        /*//创建第一个单元格
        row_title.createCell(0).setCellValue("序号");
        row_title.createCell(1).setCellValue("用户名");
        row_title.createCell(2).setCellValue("昵称");
        row_title.createCell(3).setCellValue("真实姓名");
        row_title.createCell(4).setCellValue("性别");
        row_title.createCell(5).setCellValue("邮箱地址");
        row_title.createCell(6).setCellValue("电话号码");
        row_title.createCell(7).setCellValue("身份证号");
        row_title.createCell(8).setCellValue("状态");
        row_title.createCell(9).setCellValue("创建时间");*/

        for (int i = 0; i < list.size(); i++) {
            User user = list.get(i);
            XSSFRow row = sheet.createRow(i + 1);
//            XSSFCell cell = row.createCell(0);
//            XSSFCellStyle style = new XSSFCellStyle(null);
//            XSSFFont xssfFont = new XSSFFont(null);
//            xssfFont.setColor((short)100);
//            style.setFont(xssfFont);
//            cell.setCellStyle(style);
            row.createCell(0).setCellValue(i+1);
            row.createCell(1).setCellValue(user.getUsername());
            row.createCell(2).setCellValue(user.getPassword());
            row.createCell(3).setCellValue(user.getNickname());
            row.createCell(4).setCellValue(user.getRealname());
            if("0".equals(user.getGender())){
                row.createCell(5).setCellValue("女");
            }else if("1".equals(user.getGender())){
                row.createCell(5).setCellValue("男");
            }else{
                row.createCell(5).setCellValue("未知");
            }
            row.createCell(6).setCellValue(user.getImageUrl());
            row.createCell(7).setCellValue(user.getEmail());
            row.createCell(8).setCellValue(user.getPhone());
            row.createCell(9).setCellValue(user.getIdCard());
            if("0".equals(user.getStatus())){
                row.createCell(10).setCellValue("删除");
            }else if("1".equals(user.getStatus())){
                row.createCell(10).setCellValue("启用");
            }else if("2".equals(user.getStatus())){
                row.createCell(10).setCellValue("禁用");
            }
            row.createCell(11).setCellValue(user.getCreateTime());
//            row.createCell(12).setCellValue(user.getOpratorId());
        }

        //通过流把数据写出到本地文件
        sheets.write(os);
        //刷新
        os.flush();
        //释放资源
        os.close();
        sheets.close();
        return null;
    }

    //用户导入
    @RequestMapping("/importUser")
    @ResponseBody
    public String importUser(MultipartFile file) throws IOException {
        //创建集合
        List<User> list = new ArrayList<User>();
        //获取输入流
        InputStream is = file.getInputStream();
        //创建excel文档对象
        XSSFWorkbook sheets = new XSSFWorkbook(is);
        //获取工作表对象
        XSSFSheet sheetAt = sheets.getSheetAt(0);
        //获取头行和尾行
        int firstRowNum = sheetAt.getFirstRowNum();
        int lastRowNum = sheetAt.getLastRowNum();
        for (int i = firstRowNum+1; i <=lastRowNum ; i++) {
            //根据索引获取当前行对象
            XSSFRow row = sheetAt.getRow(i);
            User user = new User();
            String username = row.getCell(1).getStringCellValue();
            user.setUsername(username);
            String password = row.getCell(2).getStringCellValue();
            user.setPassword(password);
            String nickname = row.getCell(3).getStringCellValue();
            user.setNickname(nickname);
            String realname = row.getCell(4).getStringCellValue();
            user.setRealname(realname);
            String gender = row.getCell(5).getStringCellValue();
            if("男".equals(gender)){
                user.setGender("1");
            }else if("女".equals(gender)){
                user.setGender("0");
            }
            String email = row.getCell(7).getStringCellValue();
            user.setEmail(email);
            String phone = row.getCell(8).getStringCellValue();
            user.setPhone(phone);
            String idCard = row.getCell(9).getStringCellValue();
            user.setIdCard(idCard);
            String status = row.getCell(10).getStringCellValue();
            if("启用".equals(status)){
                user.setStatus("1");
            }else if("禁用".equals(status)){
                user.setStatus("2");
            }else if("删除".equals(status)){
                user.setStatus("0");
            }
            String createTime = row.getCell(11).getStringCellValue();
            user.setCreateTime(createTime);
            list.add(user);
//            short firstCellNum = row.getFirstCellNum();
//            short lastCellNum = row.getLastCellNum();
//            for (int j = firstCellNum+1; j < lastCellNum; j++) {
//                XSSFCell cell = row.getCell(j);
//                String value = cell.getStringCellValue();
//                System.out.println(value);
//            }
            /*//获取单元格对象
            XSSFCell cell = row.getCell(0);
            //获取当前单元格的值
            double numericCellValue = cell.getNumericCellValue();
            System.out.println(numericCellValue);*/
        }
        //批量保存数据
        userService.addBatchUser(list);
        return "success";
    }

    //用户注册
    @RequestMapping("/zhuce")
    public String zhuce(User user,String fileImg){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        user.setCreateTime(sdf.format(date));
       // user.setOpratorId(1);
        //状态  1 启用
        user.setStatus("1");
        user.setImageUrl(fileImg);
        String encode = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encode);
        userService.zhuce(user);
        return "forward:/login.jsp";
    }
    //用户页面信息展示
    @RequestMapping("/dingdan")
    public String dingdan(Model model, HttpSession session,Integer pageNo,String typeDingDan){
        if(typeDingDan==null || "".equals(typeDingDan)){
            model.addAttribute("typeDingDan","qbdd");
        }else {
            model.addAttribute("typeDingDan",typeDingDan);
        }
        if(pageNo==null){
            pageNo=1;
            model.addAttribute("sun","1");
        }else {
            model.addAttribute("sun","2");
        }
        User user =(User) session.getAttribute("userinfo");
        //全部
        PageHelper.startPage(pageNo,3);
        List<Order> list = orderService.orderList(user.getId());
        PageInfo<Order> page = new PageInfo<Order>(list);
        model.addAttribute("page",page);
        //预定
        List<Order> getyuding = orderService.getyuding(user.getId());
        PageInfo<Order> yuding = new PageInfo<Order>(getyuding);
        model.addAttribute("yuding",yuding);
        //入住
        List<Order> rzhu= orderService.getrzhu(user.getId());
        PageInfo<Order> zhu = new PageInfo<Order>(rzhu);
        model.addAttribute("zhu",zhu);

        //点评
        List<Order> tui= orderService.gettui(user.getId());
        PageInfo<Order> tuis = new PageInfo<Order>(tui);
        model.addAttribute("tui",tuis);



        model.addAttribute("user",user);
        return "forward:/wodedingdan.jsp";
    }
    //用户预修改
    @RequestMapping("/updateProyem")
    public String updateProyem(HttpSession session,Model model){
        User user =(User) session.getAttribute("userinfo");
//        User user = userService.getUserById(id);
        model.addAttribute("user",user);
        return "userUpdateYem";
    }
    //用户修改
    @RequestMapping("/updateyem")
    public String updateyem(User user,Model model,HttpSession session ,String fileImg){
//        String encode = new BCryptPasswordEncoder().encode(user.getPassword());
//        user.setPassword(encode);
        User users =(User) session.getAttribute("userinfo");
        user.setId(users.getId());
        user.setImageUrl(fileImg);
          userService.updateUseryem(user);
        User userById = userService.getUserById(users.getId());
        model.addAttribute("user",userById);
        return dingdan(model,session,1,"qbdd");
//        return getPage(model);
    }

    //用户密码预修改
    @RequestMapping("/updateProword")
    public String updateProword(HttpSession session,Model model){
        User user =(User) session.getAttribute("userinfo");
//        User user = userService.getUserById(id);
        model.addAttribute("user",user);
        return "userUpwordYem";
    }
    //页面密码修改
    @RequestMapping("/updateword")
    public String updateword(User user,Model model,HttpSession session){
        User users =(User) session.getAttribute("userinfo");
        user.setId(users.getId());
        String encode = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encode);
        userService.updateUserword(user);
        User userById = userService.getUserById(users.getId());
        model.addAttribute("user",userById);
        return "forward:/wodedingdan.jsp";
//        return getPage(model);
    }
    //用户通过用户名 身份证号找回密码
    @RequestMapping("/selectyong")
    public String selectyong(User user,HttpSession session){
        User users = userService.getyong(user);
        if(users==null){
            return "forward:/loginss.jsp";
        }else {
            session.setAttribute("pass",users);
            return "userUpwordYem";
        }
    }
    //页面密码修改
    @RequestMapping("/updatepass")
    public String updatepass(User user,Model model,HttpSession session){
        User users =(User) session.getAttribute("pass");
        user.setId(users.getId());
        String encode = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encode);
        userService.updateUserword(user);
        User userById = userService.getUserById(users.getId());
        model.addAttribute("user",userById);
        return "forward:/login.jsp";
//        return getPage(model);
    }

    //异步验证用户名唯一
    @RequestMapping("/nameone")
    @ResponseBody
    public String nameone(String name){
        User user = userService.nameone(name);
        if(user==null){
            return "1";
        }
          return "2";
    }
    //异步验证身份证唯一
    @RequestMapping("/cardone")
    @ResponseBody
    public String cardone(String card){
        User user = userService.cardone(card);
        if(user==null){
            return "1";
        }
        return "2";
    }
    @RequestMapping("/yudingfj")
    public String yudingfj(int id ,Model model ,HttpSession session){
        kfRoomService.yuadd(id);
        return dingdan(model,session,1,"qbdd");
    }

    //重载一下
    public String getPage(Model model){
        return getPage(model,1);
    }
}

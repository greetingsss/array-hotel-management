package com.my.hotel.controller;

import com.alibaba.fastjson.JSON;
import com.my.hotel.entity.Order;
import com.my.hotel.service.KfRoomService;
import com.my.hotel.service.OrderService;
import com.my.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = "/stat",produces = "text/html;charset=utf-8")
public class StatisicsController {

    @Autowired
    private UserService userService;
    @Autowired
    private KfRoomService kfRoomService;
    @Autowired
    private OrderService orderService;
    //跳转到统计页面
    @RequestMapping("/toStat")
    public String toStat(){
        return "statistics";
    }

    //查询用户的男女数据的统计图
    @RequestMapping("/getGender")
    @ResponseBody
    public String getGender(){
        // gender =0  count =7   gender =1  count=11
        List<Map> list = userService.getGender();
        ArrayList<Map<String, String>> lists = new ArrayList<Map<String, String>>();
        for (Map map : list) {
            HashMap<String, String> maps = new HashMap<String, String>();
            String gender =(String) map.get("gender");
            Long count =(Long) map.get("count");
            if("0".equals(gender)){
                maps.put("name","女");
                maps.put("value",String.valueOf(count));
            }else if("1".equals(gender)){
                maps.put("name","男");
                maps.put("value",String.valueOf(count));
            }
            lists.add(maps);
        }
        String s = JSON.toJSONString(lists);
        System.out.println(s);
        return s;
    }

    //查询客房类别对应房间的平均价格
    @RequestMapping("/getAvg")
    @ResponseBody
    public String getAvg(){
        // {name:"['大床房','总统房','情侣房']",price:"[100,200,300]"}
        List<Map> list = kfRoomService.getAvg();
        ArrayList<String> listName = new ArrayList<String>();
        ArrayList<Double> listPrice = new ArrayList<Double>();
        for (Map map : list) {
            String name =(String) map.get("name");
            Double price =(Double) map.get("price");
            listName.add(name);
            listPrice.add(price);
        }
//        String name = JSON.toJSONString(listName);
//        String price = JSON.toJSONString(listPrice);
        HashMap<String, List> map = new HashMap<String, List>();
        map.put("name",listName);
        map.put("price",listPrice);
        return JSON.toJSONString(map);
    }

    //查询每月总金额
    @RequestMapping("/getorder")
    @ResponseBody
    public String getorder(){
        List<Order> list = orderService.getList();
        ArrayList<String>listmonth  = new ArrayList<String>();
        ArrayList<Double> listmoney  = new ArrayList<Double>();
        for (Order order : list) {
            double money = order.getMoney();//金额
            String orderMonth = order.getOrderMonth();//月份
           String orders=orderMonth+"月";
            listmoney.add(money);
            listmonth.add(orders);
        }
        HashMap<String, List> map = new HashMap<String, List>();
        map.put("money",listmoney);
        map.put("ordermonth",listmonth);
        return JSON.toJSONString(map);
    }
}

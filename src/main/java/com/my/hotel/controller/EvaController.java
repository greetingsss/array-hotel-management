package com.my.hotel.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.*;
import com.my.hotel.service.EvaService;
import com.my.hotel.service.EvareplyService;
import com.my.hotel.service.KfTypeService;
import com.my.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/eva",produces = "text/html;charset=utf-8")
public class EvaController {
    @Autowired
    private EvaService evaService;
    @Autowired
    private EvareplyService evareplyService;
    @Autowired
    private KfTypeService kfTypeService;
  //模糊
    @RequestMapping("/getPage")
    public String getPage(Integer chuang,Integer rshu,Model model,Integer pageNo,Integer dingwie){
        if(pageNo==null){
            pageNo=1;
        }

        if(chuang==null){
            chuang=0;
        }
        if(rshu==null){
            rshu=0;
        }
        PageHelper.startPage(pageNo,10);
        List<Eva> list = evaService.getList();
        PageInfo<Eva> page = new PageInfo<Eva>(list);
        //酒店信息
        List<KfType> kytype = kfTypeService.getmohu(chuang, rshu);
        PageInfo<KfType> pages = new PageInfo<KfType>(kytype);
        model.addAttribute("type",pages);
        model.addAttribute("page",page);
      //  model.addAttribute("jiudian",2);
        System.out.println(dingwie);
        if(dingwie==null){
            model.addAttribute("dingwie","10");
        }else if(dingwie==2){
            model.addAttribute("dingwie","11");
        }else if(dingwie==3){
            model.addAttribute("dingwie","12");
        }else if(dingwie==4){
            model.addAttribute("dingwie","13");
        }else if(dingwie==1){
            model.addAttribute("dingwie","10");
        }
        return "forward:/index.jsp";
    }


    @RequestMapping("/evacount")
    //异步提交
    @ResponseBody
    public String evacount(){
       String num = String.valueOf( evaService.EvaCount());

        return num;
    }
    //时间排序
    @RequestMapping("/getshijian")
    public String getshijian(Model model,Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Eva> list = evaService.getshijian();
        PageInfo<Eva> page = new PageInfo<Eva>(list);

        //酒店信息
        List<KfType> kytype = kfTypeService.getList();
        PageInfo<KfType> pages = new PageInfo<KfType>(kytype);
        model.addAttribute("type",pages);
        model.addAttribute("page",page);
        model.addAttribute("pingjia","1");
        return "forward:/index.jsp";
    }


    //热门排序
    @RequestMapping("/getmen")
    public String getmen(Model model,Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Eva> list = evaService.getmen();
        PageInfo<Eva> page = new PageInfo<Eva>(list);

        //酒店信息
        List<KfType> kytype = kfTypeService.getList();
        PageInfo<KfType> pages = new PageInfo<KfType>(kytype);
        model.addAttribute("type",pages);
        model.addAttribute("page",page);
        model.addAttribute("pingjia","1");
        return "forward:/index.jsp";
    }

    //好排序
    @RequestMapping("/gethaoping")
    public String gethaoping(Model model,Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Eva> list = evaService.gethaoping();
        PageInfo<Eva> page = new PageInfo<Eva>(list);
        //酒店信息
        List<KfType> kytype = kfTypeService.getList();
        PageInfo<KfType> pages = new PageInfo<KfType>(kytype);
        model.addAttribute("type",pages);

        model.addAttribute("page",page);
        model.addAttribute("pingjia","1");
        return "forward:/index.jsp";
    }

    //中评
    @RequestMapping("/getzhongping")
    public String getzhongping(Model model,Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Eva> list = evaService.getzhongping();
        PageInfo<Eva> page = new PageInfo<Eva>(list);

        //酒店信息
        List<KfType> kytype = kfTypeService.getList();
        PageInfo<KfType> pages = new PageInfo<KfType>(kytype);
        model.addAttribute("type",pages);

        model.addAttribute("page",page);
        model.addAttribute("pingjia","1");
        return "forward:/index.jsp";
    }
    //差评
    @RequestMapping("/getchagping")
    public String getchagping(Model model,Integer pageNo){

        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Eva> list = evaService.getchaping();
        PageInfo<Eva> page = new PageInfo<Eva>(list);

        //酒店信息
        List<KfType> kytype = kfTypeService.getList();
        PageInfo<KfType> pages = new PageInfo<KfType>(kytype);
        model.addAttribute("type",pages);
        model.addAttribute("page",page);
        model.addAttribute("pingjia","1");
        return "forward:/index.jsp";
    }

    //管理员操作
    @RequestMapping("/getList")
    public String getList(Model model,Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Eva> list = evaService.getList();
        PageInfo<Eva> page = new PageInfo<Eva>(list);
        model.addAttribute("page",page);
        return "/EvaPage";
    }
    //管理员操作删除
    @RequestMapping("/deleva")
    public String deleva(Model model, int id){
        evaService.deleteEva(id);
        return getList(model);
    }


    //回评
    @RequestMapping("/evareply")
    @ResponseBody
    public String evareply(String ccc, HttpSession session,Model model,int kfid){
        EvaReply reply = new EvaReply();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        User user =(User) session.getAttribute("userinfo");
        reply.setEvaId(user.getId());
        reply.setReplyTime(sdf.format(date));
        reply.setKfId(kfid);
        reply.setReplyContent(ccc);
        evareplyService.addreply(reply);
        return getshijian(model);
    }

    //点赞
    @RequestMapping("/dingzhua")
    public String dingzhua(int kfid,Model model,HttpSession session){
        User user =(User) session.getAttribute("userinfo");
        Evahun getdian = evaService.getdian(user.getId(), kfid);
        if(getdian==null){
            int getshu = evaService.getshu(kfid);
             evaService.addshu(user.getId(),kfid);
            int count=getshu+1;
            Eva eva = new Eva();
            eva.setKfId(kfid);
            eva.setEvaMen(String.valueOf(count) );
            evaService.kfupdate(eva);
        }else {
            int getshu = evaService.getshu(kfid);
            evaService.hunupdate(user.getId(),kfid);
            int count=getshu-1;
            Eva eva = new Eva();
            eva.setKfId(kfid);
            eva.setEvaMen(String.valueOf(count) );
            evaService.kfupdate(eva);
        }
        return getshijian(model,1);
    }
    //评论
    @RequestMapping("/ping")
    public String ping(String kfId,String orderId,Model model){
        model.addAttribute("kfId",kfId);
        model.addAttribute("orderId",orderId);
        return "eva";
    }
    //添加评论
    @RequestMapping("/addEva")
    public String addEva(Eva eva,String imageUrl,String orderId){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        eva.setEvaTime(sdf.format(date));
        eva.setAppStatus("0");
        eva.setEvaMen("0");
        evaService.addEva(eva,imageUrl,orderId);

        return "success";
    }




    //重载一下
    public String getList(Model model){
        return getList(model,1);
    }

    public String getshijian(Model model){
        return getshijian(model,1);
    }

}

package com.my.hotel.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Hd;
import com.my.hotel.entity.KfRoom;
import com.my.hotel.entity.KfType;
import com.my.hotel.service.HdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/hd")
public class HdController {
    @Autowired
    private HdService hdService;

    //分页查询
    @RequestMapping("/hdPage")
    public String getPage(Model model, Integer pageNo){
        if(pageNo==null){
            pageNo=1;
        }
        PageHelper.startPage(pageNo,10);
        List<Hd> list = hdService.getList();
        PageInfo<Hd> page = new PageInfo<Hd>(list);
        model.addAttribute("page",page);
        return "hdPage";
    }
    //活动信息预添加
    @RequestMapping("/addPreHd")
    public String addPreHd(Model model){
        return "hdAdd";
    }
    //活动信息添加
    @RequestMapping("/addHd")
    public String addHd(Hd hd,Model model){
        hdService.addHd(hd);
        return getPage(model);
    }
    //活动信息删除
    @RequestMapping("/delHd")
    public String delHd(int id,Model model){
        hdService.deleteHd(id);
        return getPage(model);
    }
    //活动信息预修改
    @RequestMapping("/updPreHd")
    public String updPreHd(int id,Model model){
        Hd hd = hdService.getHdById(id);
        model.addAttribute("hd",hd);
        return "hdUpdate";
    }
    //活动信息修改
    @RequestMapping("/updHd")
    public String updHd(Hd hd,Model model){
        hdService.updateHd(hd);
        return getPage(model);
    }

    //根据id查询活动信息
    @RequestMapping("/getHdById")
    @ResponseBody
    public String getHdById(int id){
        Hd hd = hdService.getHdById(id);
        String hdStr = JSON.toJSONString(hd);
        return hdStr;
    }

    public String getPage(Model model){
        return getPage(model,1);
    }
}


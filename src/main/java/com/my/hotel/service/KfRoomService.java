package com.my.hotel.service;

import com.my.hotel.entity.KfRoom;
import com.my.hotel.entity.TbRoom;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface KfRoomService {

    //查询
    public List<KfRoom> getList();

    //新增
    public void addKfRoom(KfRoom kfRoom,String fileImg);

    //根据id查询对象信息
    public KfRoom getKfRoomById(int id);

    //修改
    public void updateKfRoom(KfRoom kfRoom);

    //删除
    public void deleteKfRoom(int id);

    public List<Map> getAvg();

    //根据客服id查询房间
    public List <KfRoom> roomList(int kfTypeId);
    //用户预定房间
    public void yuadd(int kfId);

    //管理员查询订单
    public List<KfRoom> selectding( int id);



}

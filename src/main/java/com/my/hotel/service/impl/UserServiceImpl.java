package com.my.hotel.service.impl;

import com.my.hotel.entity.Role;
import com.my.hotel.entity.User;
import com.my.hotel.entity.UserRole;
import com.my.hotel.mapper.RoleMapper;
import com.my.hotel.mapper.UserMapper;
import com.my.hotel.mapper.UserRoleMapper;
import com.my.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RoleMapper roleMapper;


    public List<User> getList() {
        return userMapper.getList();
    }

    public void addUser(User user){
        userMapper.addUser(user);
    }

    public void delUser(int id) {
        userMapper.delUser(id);
    }

    public User getUserById(int id) {
        return userMapper.getUserById(id);
    }

    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    public void addUserRole(List<UserRole> list) {
        //添加用户关联角色数据之前 需要先删除用户关联角色的所有数据
        userRoleMapper.delUserRole(list.get(0).getUserId());
        userRoleMapper.addUserRole(list);
    }

    public User getUserByUserName(String username) {
        return userMapper.getUserByUserName(username);
    }

    public List<Map> getGender() {
        return userMapper.getGender();
    }

    public void addBatchUser(List<User> list) {
        userMapper.addBatchUser(list);
    }
  //用户注册
   @Transactional //添加事物
    public void zhuce(User user) {
        //添加用户
        userMapper.addUser(user);
        int id = user.getId();
       System.out.println(id);
        //客户对应的id
        Role role = roleMapper.getone();
        UserRole userRole = new UserRole();
       userRole.setUserId(id);
        userRole.setRoleId(role.getId());
        userRoleMapper.adduser(userRole);
    }
    //查询用户对应的角色
    public UserRole getRole(int userId) {
        return userRoleMapper.getRole(userId);
    }
//修改页面个人信息
    public void updateUseryem(User user) {
        userMapper.updateUseryem(user);
    }
  //修改页面个人信息密码
    public void updateUserword(User user) {
        userMapper.updateUserword(user);
    }
    //通过用户名和身份证找回密码
    public User getyong(User user) {
        return userMapper.getyong(user);
    }
       //异步验证用户名
    public User nameone(String username) {
        return userMapper.nameone(username);
    }
       //异步验证身份证号唯一
    public User cardone(String idCard) {
        return userMapper.cardone(idCard);
    }


}

package com.my.hotel.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Role;
import com.my.hotel.entity.RoleMenu;
import com.my.hotel.mapper.RoleMapper;
import com.my.hotel.mapper.RoleMenuMapper;
import com.my.hotel.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    public PageInfo<Role> getPage(int pageNo) {
        PageHelper.startPage(pageNo,10);
        List<Role> list = roleMapper.getList();
        PageInfo<Role> page = new PageInfo<Role>(list);
        return page;
    }

    public Role getRoleById(int id) {
        return roleMapper.getRoleById(id);
    }

    public void updRole(Role role) {
        roleMapper.updRole(role);
    }

    public void addRole(Role role) {
        roleMapper.addRole(role);
    }

    public void delRole(int id) {
        roleMapper.delRole(id);
    }

    public List<Role> getDistRole(int userId) {
        return roleMapper.getDistRole(userId);
    }

    public List<Role> getUnDistRole(int userId) {
        return roleMapper.getUnDistRole(userId);
    }

    public void addRoleMenu(ArrayList<RoleMenu> list) {
        //先删除现有的 角色所有用的菜单
        roleMenuMapper.delRoleMenu(list.get(0).getRoleId());
        //添加
        roleMenuMapper.addRoleMenu(list);
    }
}

package com.my.hotel.service.impl;

import com.my.hotel.entity.EvaReply;
import com.my.hotel.mapper.EvareplyMapper;
import com.my.hotel.service.EvareplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvareplyServiceImpl implements EvareplyService {

    @Autowired
    private EvareplyMapper evareplyMapper;
    public void addreply(EvaReply evaReply) {
        evareplyMapper.addreply(evaReply);
    }
}

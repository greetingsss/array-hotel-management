package com.my.hotel.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Menu;
import com.my.hotel.entity.Role;
import com.my.hotel.mapper.MenuMapper;
import com.my.hotel.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    public PageInfo<Menu> getPage(int pageNo) {
        PageHelper.startPage(pageNo,10);
        List<Menu> list = menuMapper.getList();
        for (Menu menu : list) {
            int pid = menu.getMenuPid();
            Menu pMenu = menuMapper.getMenuById(pid);
            menu.setPmenu(pMenu);
        }
        PageInfo<Menu> page = new PageInfo<Menu>(list);
        return page;
    }

    public List<Menu> getList(){
        return menuMapper.getList();
    }



    public Menu getMenuById(int id) {
        return menuMapper.getMenuById(id);
    }

    public void updMenu(Menu menu) {
        menuMapper.updMenu(menu);
    }

    public void addMenu(Menu menu) {
        menuMapper.addMenu(menu);
    }

    public void delMenu(int id) {
        menuMapper.delMenu(id);
    }

    public List<Menu> getDistMenu(int roleId) {
        return menuMapper.getDistMenu(roleId);
    }

    public List<Menu> getUnDistMenu(int roleId) {
        return menuMapper.getUnDistMenu(roleId);
    }

   /* public List<Menu> getMenuList(int userId) {
        //查询顶级菜单
        List<Menu> list = menuMapper.getMenuList(userId);
        for (Menu menu : list) {
            int id = menu.getId();
            List<Menu> cList = menuMapper.getClist(id);
            menu.setClist(cList);
            for (Menu menu1 : cList) {
                int id1 = menu1.getId();
                List<Menu> cclist = menuMapper.getClist(id1);
                menu1.setClist(cclist);
            }
        }
        return list;
    }*/


    public List<Menu> getMenuList(int userId) {
        //查询顶级菜单
        List<Menu> list = menuMapper.getMenuList2(userId,0);
        getClist(list,userId);
        return list;
    }

    //递归实现
    public void getClist(List<Menu> list,int userId){
        if(list !=null){
            for (Menu menu : list) {
                List<Menu> clist = menuMapper.getMenuList2(userId,menu.getId());
                menu.setClist(clist);
                getClist(clist,userId);
            }
        }
    }
}

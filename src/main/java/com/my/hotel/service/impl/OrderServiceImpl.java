package com.my.hotel.service.impl;

import com.my.hotel.entity.Order;
import com.my.hotel.entity.TbRoom;
import com.my.hotel.entity.User;
import com.my.hotel.mapper.OrderMapper;
import com.my.hotel.mapper.UserMapper;
import com.my.hotel.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserMapper userMapper;


    //查询完成订单
    public List<Order> getList() {
        return orderMapper.getList();
    }
     //根据order_status查询订单状态
    @Transactional
    public List<Order> getstatus(int orderStatus) {
        List<Order> orders = orderMapper.getstatus(orderStatus);
        for (Order order : orders) {
            User user = userMapper.getUserById(order.getUserId());
            TbRoom roomone = orderMapper.roomone(order.getKfId());
            order.setUser(user);
            order.setTbRoom(roomone);
        }
        return orders;
    }
  //管理员确认订单
    @Transactional //添加事物
    public void ordupdete(int id,int kfId) {
        orderMapper.ordupdete(id);
        orderMapper.roomupdate(kfId);
    }
    //查询所有订单
    public List<Order> orderList(int userId) {
        return orderMapper.orderList(userId);
    }
    @Transactional //添加事物  预定房间
    public void orderadd(Order order) {
     order.setOrderStatus("0");
     order.setOrderPayStatus("0");
        String time = order.getOrderYdTime();
        String[] split = time.split("-");
        order.setOrderMonth(split[1]);
        int maxs = orderMapper.selone();
        int max=maxs+1;
        order.setOrderNum(String.valueOf(max));
        orderMapper.orderadd(order);
         orderMapper.updateroom(order.getKfId());
    }

//预定
    public List<Order> getyuding(int userId) {
        return orderMapper.getyuding(userId);
    }
//入住
    public List<Order> getrzhu(int userId) {
        return orderMapper.getrzhu(userId);
    }
//退房
    public List<Order> gettui(int userId) {
        return orderMapper.gettui(userId);
    }
    @Transactional //添加事物
    public void ordwang(int id,int kfId) {
        orderMapper.ordwang(id);
        orderMapper.roomwang(kfId);
    }


}

package com.my.hotel.service.impl;

import com.my.hotel.entity.Log;
import com.my.hotel.entity.User;
import com.my.hotel.mapper.LogMapper;
import com.my.hotel.mapper.UserMapper;
import com.my.hotel.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;
    @Autowired
    private UserMapper userMapper;
    //添加
    public void addLog(Log log) {
        logMapper.addLog(log);
    }
    //查询
    public List<Log> getPage() {
        List<Log> page = logMapper.getPage();
//        ArrayList<Log> logs = new ArrayList<Log>();
        for (Log log : page) {
            User user = userMapper.getUserById(log.getOptId());
            log.setUser(user);
//            logs.add(log);
        }
        return page;
    }
}

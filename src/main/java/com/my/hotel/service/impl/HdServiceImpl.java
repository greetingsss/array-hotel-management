package com.my.hotel.service.impl;

import com.my.hotel.entity.Hd;
import com.my.hotel.mapper.HdMapper;
import com.my.hotel.service.HdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HdServiceImpl implements HdService {
    @Autowired
    private HdMapper hdMapper;

    public List<Hd> getList() {
        return hdMapper.getList();
    }

    public void addHd(Hd hd) {
        hdMapper.addHd(hd);
    }

    public Hd getHdById(int id) {
        return hdMapper.getHdById(id);
    }

    public void updateHd(Hd hd) {
        hdMapper.updateHd(hd);
    }

    public void deleteHd(int id) {
        hdMapper.deleteHd(id);
    }

    public List<Hd> getListByNow(String now) {
        return hdMapper.getListByNow(now);
    }
}

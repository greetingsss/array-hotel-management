package com.my.hotel.service.impl;

import com.my.hotel.entity.ExcelDTO;
import com.my.hotel.mapper.ExcelMapper;
import com.my.hotel.service.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    private ExcelMapper excelMapper;
    public List<ExcelDTO> getList(String tableName) {
        return excelMapper.getList(tableName);
    }
}

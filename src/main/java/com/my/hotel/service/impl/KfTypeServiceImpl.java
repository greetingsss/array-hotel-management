package com.my.hotel.service.impl;

import com.my.hotel.entity.KfType;
import com.my.hotel.mapper.KfTypeMapper;
import com.my.hotel.service.KfTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KfTypeServiceImpl implements KfTypeService {

    @Autowired
    private KfTypeMapper kfTypeMapper;

    public List<KfType> getList() {
        return kfTypeMapper.getList();
    }

    public void addKfType(KfType kfType) {
        kfTypeMapper.addKfType(kfType);
    }

    public void updateKfType(KfType kfType) {
        kfTypeMapper.updateKfType(kfType);
    }

    public KfType getKfTypeById(int id) {
        return kfTypeMapper.getKfTypeById(id);
    }

    public void deleteKfType(int id) {
        kfTypeMapper.deleteKfType(id);
    }

    public List<KfType> getmohu(int typeChuang, int typeShu) {
        return kfTypeMapper.getmohu(typeChuang,typeShu);
    }
}

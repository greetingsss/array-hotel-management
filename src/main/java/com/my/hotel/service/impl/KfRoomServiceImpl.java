package com.my.hotel.service.impl;

import com.my.hotel.entity.KfImg;
import com.my.hotel.entity.KfRoom;
import com.my.hotel.entity.KfType;
import com.my.hotel.entity.TbRoom;
import com.my.hotel.mapper.KfImgMapper;
import com.my.hotel.mapper.KfRoomMapper;
import com.my.hotel.mapper.KfTypeMapper;
import com.my.hotel.service.KfRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class KfRoomServiceImpl implements KfRoomService {

    @Autowired
    private KfRoomMapper kfRoomMapper;
    @Autowired
    private KfImgMapper kfImgMapper;

    @Autowired
    private KfTypeMapper kfTypeMapper;

    public List<KfRoom> getList() {
        List<KfRoom> list = kfRoomMapper.getList();
        for (KfRoom kfRoom : list) {
            int kfTypeId = kfRoom.getKfTypeId();
            KfType kfType = kfTypeMapper.getKfTypeById(kfTypeId);
            kfRoom.setKfType(kfType);
        }
        return list;
    }

    @Transactional
    public void addKfRoom(KfRoom kfRoom,String fileImg) {
        kfRoomMapper.addKfRoom(kfRoom);
        int kfId = kfRoom.getId();
        KfImg kfImg = new KfImg();
        kfImg.setKfId(kfId);
        kfImg.setKfImgPath(fileImg);
        kfImgMapper.addKfImg(kfImg);

        int max = kfRoomMapper.maxroom(kfRoom.getKfTypeId());
        TbRoom room = new TbRoom();
        if(max==1){
         String num= kfRoom.getKfTypeId()+"01";
            room.setRoomNum(num);
        }else {
            room.setRoomNum( String.valueOf(max+1));
        }

        room.setRoomStatus(0);
        room.setKfId(kfId);
        kfRoomMapper.addroom(room);
    }

    public KfRoom getKfRoomById(int id) {
        return kfRoomMapper.getKfRoomById(id);
    }

    public void updateKfRoom(KfRoom kfRoom) {
        kfRoomMapper.updateKfRoom(kfRoom);
    }
    @Transactional
    public void deleteKfRoom(int id) {
        kfRoomMapper.deleteKfRoom(id);
        kfRoomMapper.delectroom(id);

    }

    public List<Map> getAvg() {
        return kfRoomMapper.getAvg();
    }

   //根据客房id查询
    public List<KfRoom> roomList(int kfTypeId) {
        List<KfRoom> kfRooms = kfRoomMapper.roomList(kfTypeId);
        for (KfRoom kfRoom : kfRooms) {
            KfImg getimg = kfImgMapper.getimg(kfRoom.getId());
            kfRoom.setKfImg(getimg);
            TbRoom getkong = kfRoomMapper.getkong(kfRoom.getId());
            kfRoom.setTbRoom(getkong);
            KfType type = kfTypeMapper.getKfTypeById(kfTypeId);
            kfRoom.setKfType(type);
        }
        return kfRooms;
    }
   //用户预定房间
    public void yuadd(int kfId) {
        kfRoomMapper.yuadd(kfId);
    }
   //管理员管理订单
    public List<KfRoom> selectding(int id) {
        List<KfRoom> rooms = kfRoomMapper.selectding();
        for (KfRoom room : rooms) {
            KfType kftype = kfTypeMapper.getKfTypeById(room.getKfTypeId());
            TbRoom selectone = kfRoomMapper.selectone(room.getId(), id);
            room.setKfType(kftype);
            room.setTbRoom(selectone);
        }
        return rooms;
    }



}

package com.my.hotel.service.impl;

import com.my.hotel.entity.*;
import com.my.hotel.mapper.*;
import com.my.hotel.service.EvaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
@Service
public class EvaServiceImpl implements EvaService {

    @Autowired
    private EvaMapper evaMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private KfTypeMapper kfTypeMapper;
    @Autowired
    private EvareplyMapper evareplyMapper;
    @Autowired
    private OrderMapper orderMapper;

    //查询
    public List<Eva> getList() {
        List<Eva> evas = evaMapper.getList();
        for (Eva eva : evas) {
            User user = userMapper.getUserById(eva.getUserId());
            KfType kfType = kfTypeMapper.getKfTypeById(eva.getKfId());
            List<EvaReply> list = evareplyMapper.getList(eva.getKfId());
            Evaimg aeleva = evaMapper.aeleva(eva.getId());
            eva.setEvaReply(list);
            eva.setKfType(kfType);
            eva.setUser(user);
            eva.setEvaimg(aeleva);
        }
        return evas;
//        return evaMapper.getList();
    }
    //删除
    public void deleteEva(int id) {
     evaMapper.deleteEva(id);
    }
   //总条数
    public int EvaCount() {
        return evaMapper.EvaCount();
    }
     //时间排序
    public List<Eva> getshijian() {
        List<Eva> evas = evaMapper.getshijian();
        for (Eva eva : evas) {
            User user = userMapper.getUserById(eva.getUserId());
            List<EvaReply> list = evareplyMapper.getList(eva.getKfId());
            eva.setEvaReply(list);
            eva.setUser(user);
        }
        return evas;
    }
     //好评排序
    public List<Eva> gethaoping() {
        List<Eva> evas = evaMapper.gethaoping();
        for (Eva eva : evas) {
            User user = userMapper.getUserById(eva.getUserId());
            List<EvaReply> list = evareplyMapper.getList(eva.getKfId());
            eva.setEvaReply(list);
            eva.setUser(user);
        }
        return evas;
    }
   //中评
    public List<Eva> getzhongping() {
        List<Eva> evas = evaMapper.getzhongping();
        for (Eva eva : evas) {
            User user = userMapper.getUserById(eva.getUserId());
            List<EvaReply> list = evareplyMapper.getList(eva.getKfId());
            eva.setEvaReply(list);
            eva.setUser(user);
        }
        return evas;
    }
  //差评
    public List<Eva> getchaping() {
        List<Eva> evas = evaMapper.getchaping();
        for (Eva eva : evas) {
            User user = userMapper.getUserById(eva.getUserId());
            List<EvaReply> list = evareplyMapper.getList(eva.getKfId());
            eva.setEvaReply(list);
            eva.setUser(user);
        }
        return evas;
    }
 //热门排序
    public List<Eva> getmen() {
        List<Eva> evas = evaMapper.getmen();
        for (Eva eva : evas) {
            User user = userMapper.getUserById(eva.getUserId());
            List<EvaReply> list = evareplyMapper.getList(eva.getKfId());
            eva.setEvaReply(list);
            eva.setUser(user);
        }
        return evas;
    }

    //查询用户对每个房间的点赞
    public Evahun getdian(int userId, int kfId) {
        return evaMapper.getdian(userId,kfId);
    }
    //查询房间的点赞数
    public int getshu(int kfId) {
        return evaMapper.getshu(kfId);
    }
    //修改点赞数量
    public void kfupdate(Eva eva) {
    evaMapper.kfupdate(eva);
    }
  //添加热门表数据
    public void addshu(int userId, int kfId) {
        evaMapper.addshu(userId,kfId);
    }
//删除热门表格数据
    public void hunupdate(int userId, int kfId) {
     evaMapper.hunupdate(userId,kfId);
    }
//添加评论
     @Transactional //添加事物
    public void addEva(Eva eva,String imageUrl,String orderId) {
        evaMapper.addEva(eva);
        int id = eva.getId();
        Evaimg evaimg = new Evaimg();
        evaimg.setEvaId(id);
        evaimg.setEvaImgPath(imageUrl);
        evaMapper.addevaimg(evaimg);
        orderMapper.upping(Integer.parseInt(orderId));
    }
}

package com.my.hotel.service;

import com.my.hotel.entity.Log;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LogService {
    //添加日志
    public void addLog(Log log);
    //查询日志
    public List<Log> getPage();
}

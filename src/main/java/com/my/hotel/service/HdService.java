package com.my.hotel.service;

import com.my.hotel.entity.Hd;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface HdService {

    //查询
    public List<Hd> getList();

    //新增
    public void addHd(Hd hd);

    //根据id查询对象信息
    public Hd getHdById(int id);

    //修改
    public void updateHd(Hd hd);

    //删除
    public void deleteHd(int id);
    // 根据当前系统时间查询活动列表
    public List<Hd> getListByNow(String now);
}

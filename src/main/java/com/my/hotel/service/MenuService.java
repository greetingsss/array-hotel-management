package com.my.hotel.service;

import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Menu;
import com.my.hotel.entity.Role;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface MenuService {

    //查询所有菜单
    public PageInfo<Menu> getPage(int pageNo);

    public List<Menu> getList();

    //根据id查询菜单对象信息
    public Menu getMenuById(int id);

    //修改菜单信息
    public void updMenu(Menu menu);

    //增加菜单信息
    public void addMenu(Menu menu);

    //删除菜单信息
    public void delMenu(int id);

    //查询角色分配的菜单
    public List<Menu> getDistMenu(int roleId);
    //查询角色未分配的菜单
    public List<Menu> getUnDistMenu(int roleId);

    //根据用户id查询所有的菜单列表
    public List<Menu> getMenuList(int userId);
}

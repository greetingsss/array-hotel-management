package com.my.hotel.service;

import com.my.hotel.entity.User;
import com.my.hotel.entity.UserRole;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface UserService {
    //查询所有用户数据
    public List<User> getList();
    //添加用户数据
    public void addUser(User user);
    //删除用户数据
    public void delUser(int id);
    //根据id查询用户对象数据
    public User getUserById(int id);
    //修改用户数据
    public void updateUser(User user);
    //添加用户角色关联表数据
    public void addUserRole(List<UserRole> list);

    //通过用户名查询用户信息
    public User getUserByUserName(String username);

    public List<Map> getGender();

    //批量添加
    public void addBatchUser(List<User> list);

    //用户注册
    public void zhuce(User user);

    //查询用户对应的角色
    public UserRole getRole( int userId);
    //页面个人信息修改
    public void updateUseryem(User user);

    //修改页面个人信息密码
    public void updateUserword(User user);

    //通过 用户名，身份证 找回密码
    public User getyong(User user);

    //异步验证用户名唯一
    public User nameone(String username);

    //异步验证身份证号唯一
    public User cardone(String idCard);
}

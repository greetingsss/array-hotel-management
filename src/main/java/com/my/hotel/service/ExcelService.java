package com.my.hotel.service;

import com.my.hotel.entity.ExcelDTO;

import java.util.List;

public interface ExcelService {

    public List<ExcelDTO> getList(String tableName);
}

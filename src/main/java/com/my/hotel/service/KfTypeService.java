package com.my.hotel.service;

import com.my.hotel.entity.KfType;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface KfTypeService {

    //查询
    public List<KfType> getList();

    //新增
    public void addKfType(KfType kfType);

    //修改
    public void updateKfType(KfType kfType);

    //根据id查询对象信息
    public KfType getKfTypeById(int id);

    //删除
    public void deleteKfType(int id);

    //模糊查询
    public List<KfType> getmohu(@Param("typeChuang") int typeChuang, @Param("typeShu") int typeShu);
}

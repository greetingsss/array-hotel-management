package com.my.hotel.service;

import com.my.hotel.entity.Eva;
import com.my.hotel.entity.Evahun;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface EvaService {
    //查询
    public List<Eva> getList();

    //删除
    public void deleteEva(int id);
  //查询总条数
    public int EvaCount();
    //时间排序
    public List<Eva> getshijian();

    //好评排序
    public List<Eva> gethaoping();
    //中评
    public List<Eva> getzhongping();

    //差评
    public List<Eva> getchaping();

    //热门排序
    public List<Eva> getmen();

    //查询用户对某个房间的点赞
    public Evahun getdian(@Param("userId") int userId, @Param("kfId") int kfId);
    //房间的点赞数
    public int getshu(int kfId);
    //修改点赞数量
    public void kfupdate(Eva eva);

  //添加热门表数
  public void addshu(@Param("userId") int userId, @Param("kfId") int kfId);
  //删除热门表的数据
  public void hunupdate(@Param("userId") int userId, @Param("kfId") int kfId);
 //添加评论
  public void addEva(Eva eva,String imageUrl,String orderId);
}

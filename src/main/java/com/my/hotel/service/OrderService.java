package com.my.hotel.service;

import com.my.hotel.entity.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface OrderService {
    //查询完成订单
    public List<Order> getList();

    //根据order_status查询订单状态
    public List<Order> getstatus(int orderStatus);
    //确认订单
    public void ordupdete(int id,int kfId);
    //所有订单
    public List<Order> orderList(int userId);
    //添加订单
    public void orderadd(Order order);

    //预定
    public List<Order> getyuding(int userId);
    //入住
    public List<Order> getrzhu(int userId);
    //退房
    public List<Order> gettui(int userId);

    //确认完成订单
    public void ordwang(int id,int kfId);
}

package com.my.hotel.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.my.hotel.entity.Role;
import com.my.hotel.entity.RoleMenu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.ArrayList;
import java.util.List;

public interface RoleService {

    //查询所有角色
    public PageInfo<Role> getPage(int pageNo);

    //根据id查询角色对象信息
    public Role getRoleById(int id);

    //修改角色信息
    public void updRole(Role role);

    //增加角色信息
    public void addRole(Role role);

    //删除角色信息
    public void delRole(int id);

    //查询用户已分配的角色信息
    public List<Role> getDistRole(int userId);

    //查询用户未分配的角色信息
    public List<Role> getUnDistRole(int userId);

    void addRoleMenu(ArrayList<RoleMenu> list);

}

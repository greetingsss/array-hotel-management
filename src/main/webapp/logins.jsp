
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<script src="${path}/js/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${path}js/js/login.js" type="text/javascript" charset="utf-8"></script>
		<link rel="${path}stylesheet" type="text/css" href="${path}css/logins.css"/>
	</head>
	<body>
		<div id="login">
			<div id="login_title">
				<span id="jdmc">王府井永光酒店</span>
				<a href="eva/getPage"><img src="img/fangzi.png" width="31px"/><span>首页</span></a>
			</div>
			<div id="img">
				<img src="/img/login001.jpg"/>
				<div id="img_content">
					<form action="/user/zhuce" method="get">
						<table cellpadding="20px"  >
							<tr>
								<td colspan="2" id="img_content_zc">
									<span>
										注册
									</span>
									<a href="login.jsp">去登录……</a>
								</td>
							</tr>
							<tr>
								<td class="fistTr">用户名：</td>
								<td class="sTr"><input type="text" name="username" id="username" onchange="yanzhen()" required="required" /></td>
							</tr>

							<tr>
								<td class="fistTr">昵称：</td>
								<td class="sTr"><input type="text" name="nickname"/></td>
							</tr>
							<tr>
								<td class="fistTr">密码：</td>
								<td class="sTr"><input type="password"  name="password" /></td>
							</tr>
							<tr>
								<td class="fistTr">真实姓名：</td>
								<td class="sTr"><input type="text" name="realname" /></td>
							</tr>
							<tr>
								<td class="fistTr">性别：</td>
								<td class="sTr">
									<input type="radio" value="1" name="gender" checked="checked"> <i></i>男
									<input type="radio" value="0" name="gender"> <i></i>女
								</td>
							</tr>
							<tr>
								<td class="fistTr">email：</td>
								<td class="sTr"><input type="text"  name="email" /></td>
							</tr>
							<tr>
								<td class="fistTr">电话号码：</td>
								<td class="sTr"><input type="text"  name="phone"/></td>
							</tr>
							<tr>
								<td class="fistTr">身份证号：</td>
								<td class="sTr"><input type="text" id="card" onchange="yansfz()" name="idCard" required="required"/></td>
							</tr>
							<tr>
								<td class="fistTr">头像：</td>
								<td class="sTr">
									<input type="file" name="imageUrl" id="imageUrl" onchange="fileadd()">
									<img src="${path}/img/1.gif" id="myImg" width="100px" height="100px">
									<input type="hidden" id="fileImg" name="fileImg">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="submit" value="注册" id="button_zc" />
									<input type="reset" value="清空" id="button_qk" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div id="login_foot">
				<img src="img/yejiao2.png"/>
			</div>
		</div>
	</body>
<script type="text/javascript">
	function fileadd(){
	var fileObj=$("#imageUrl");
	var fileEle=fileObj[0].files[0];
	var formData= new FormData();
	formData.append("file",fileEle);
	 $.ajax({
		 type:"post",
		 url:"/user/userfile",
		 data:formData,
		 dataType: "json",
		 contentType:false,
		 cache:false,
		 processData:false,
		 success:function (data) {
			 var url = data.fileName;
			 document.getElementById("myImg").src = url;
			 document.getElementById("fileImg").value = url;
		 }
	 });
	}

	function yanzhen(){
	var name=document.getElementById("username").value;
	$.ajax({
		type: "post",
		url:"/user/nameone?name="+name,
		dateType:"text",
		contentType:false,
		cache:false,
		processData:false,
		success:function (data){
			if(data==2){
				alert("该用户以注册，请重新输入")
				 document.getElementById("username").value='';
			}
		}
	});
	}
	function yansfz(){
		var cardval=document.getElementById("card").value;
		$.ajax({
			type: "post",
			url:"/user/cardone?card="+cardval,
			dateType:"text",
			contentType:false,
			cache:false,
			processData:false,
			success:function (data){
				if(data==2){
					alert("该身份证以注册，请重新输入")
					 document.getElementById("card").value='';
				}

			}
		});
	}
</script>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>酒店管理</title>
		<link rel="stylesheet" href="${path}/css/index.css">
		<script src="${path}/js/js/jquery.min.js"></script>
		<script src="${path}/js/js/lbt.js"></script>
		<script src="${path}/http://api.map.baidu.com/api?v=1.3"></script>
		<script src="${path}/js/js/ditu.js"></script>
		<script src="${path}/js/js/index.js"></script>
<%--		<link rel="/stylesheet" type="text/css" href="${path}/css/index.css"/>--%>
<%--		<script src="${path}/js/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>--%>
<%--		<script src="${path}/js/js/lbt.js" type="text/javascript" charset="utf-8"></script>--%>
<%--		<script src="http://api.map.baidu.com/api?v=1.3" type="text/javascript"></script>--%>
<%--		<script src="${path}/js/js/ditu.js" type="text/javascript" charset="utf-8"></script>--%>
<%--		<script src="${path}/js/js/index.js" type="text/javascript" charset="utf-8"></script>--%>
	</head>
	<body>
		<div id="div">
			<!--头-->
			<div id="head">
				<div id="title">
					<span id="logo"><a href="#shouyebufen">王府井永光酒店</a></span>
					<span class="dh"><a href="#shouyebufen">首页</a></span>
					<span class="dh"><a href="#fangjianyuding">房间预定</a></span>
					<span class="dh"><a href="#jiudianxiangqing">酒店详情</a></span>
					<span class="dh"><a href="#pingjia">评价</a></span>
					<span class="dh"><a href="/user/dingdan">我的订单</a></span>
					<span class="dh"><a href="/logout">退出</a></span>
<%--					<li><a href="/logout">退出登录</a>--%>
				</div>
			</div>
			<a href="" name="shouyebufen"></a>
			<div id="headTS">
				
			</div>
			
			<!--首页部分-->
			<div id="shouye">
				<div id="lbt">
					<div class="container">
				        <!-- 先把第一张图片显示出来 -->
				        <img class="on" src="/img/001.jpg" />
				        <img src="/img/002.jpg" />
				        <img src="/img/003.jpg" />
				        <img src="/img/004.jpg" />
				        <!-- 左右切换 -->
				        <div class="left"><</div>
				        <div class="right">></div>
				        <!-- 焦点 -->
				        <ul>
				            <li class="active"></li>
				            <li></li>
				            <li></li>
				            <li></li>
				        </ul>
				    </div>
				</div>
				<div id="shouye_right">
					<p id="shouye_bt">王府井永光酒店</p>
					<p>
						<span id="pf">5.0分</span>
						<span id="hp">100%好评</span>
					</p>
					<p>
						<span class="td">位置优越</span>
						<span class="td">性价比高</span>
						<span class="td">环境舒适</span>
					</p>
					<p>酒店地址：xxx省xxx市xxx街道xxx号</p>
					<p>客服电话：12345678901</p>
				</div>
			</div>
			
			<!--房间预定-->
			<a href="" name="fangjianyuding"></a>
			<div id="fjyd">
				<div id="fjyd_title" style="font-size: 48px;">
					房间预定
				</div>
				<div id="fjyd_content">
					<form action="/eva/getPage">
<%--						/kfType/getmohu?chuang=--%>
						<input type="hidden" name="dingwie" value="2">
						<select name="chuang" id="##">
							<option value="0">床型</option>
							<option value="1">大床</option>
							<option value="2">双人床</option>
							<option value="3">亲子床</option>
						</select>
						<select name="rshu" id="##1">
							<option value="0">入住人数</option>
							<option value="1">一人</option>
							<option value="2">两人</option>
							<option value="3">三人</option>
						</select>
						<input type="submit" id="fjyd_cx" value="查询" />
					</form>

					<table id="fjyd_table">
						<tr id="fjyd_tr">
							<td>图片</td>
							<td>房型</td>
							<td>床型</td>
							<td>早餐</td>
							<td>入住人数</td>
							<td>政策</td>
							<td>操作</td>
						</tr>
						<c:forEach items="${type.list}" var="type" varStatus="t">
							<tr class="fjyd_xx">
								<td><img src="/img/002.jpg"/></td>
								<td>${type.typeName}</td>
								<td>
									<c:if test="${type.typeChuang ==1}">大床</c:if>
									<c:if test="${type.typeChuang ==2}">双床</c:if>
									<c:if test="${type.typeChuang ==3}">亲子床</c:if>
								</td>
								<td>无早餐</td>
								<td>${type.typeShu}人</td>
								<td>今天12:00时钱免费取消</td>
<%--								<td class="money">￥549</td>--%>
								<td><a href="/kfRoom/yudingroom?kfid=${type.id}">预定</a></td>
							</tr>

						</c:forEach>
						<tr style="height: 100px;">
							<td colspan="8">
								<a href="${path}/eva/getPage?pageNo=1">首页</a>
								<a href="${path}/eva/getPage?pageNo=${type.isFirstPage?1:type.prePage}">上一页</a>
								<c:forEach begin="1" end="${type.pages}" varStatus="i">
									<a href="${path}/eva/getPage?pageNow=${i.count}">${i.count}</a>
								</c:forEach>
								<a href="${path}/eva/getPage?pageNo=${type.isLastPage?type.pages:type.nextPage}">下一页</a>
								<a href="${path}/eva/getPage?pageNo=${type.nextPage}">尾页</a>
								<span>${type.pageNum}/${type.pages}页</span>
								<span>共${type.total}条</span>
							</td>
						</tr>
					</table>


				</div>
			</div>
			
			<!--酒店详情-->
			<a href="" name="jiudianxiangqing"></a>
			<div id="jdxq">
				<div style="font-size: 48px;">
					酒店详情
				</div>
				<div id="jdxq_content">
					<table>
						<tr>
							<th>酒店亮点</th>
							<td>战略合作酒店，城市销量前列，优质服务保障，提供去哪儿平台专属低价</td>
						</tr>
						<tr>
							<th>联系方式</th>
							<td>+861065265558</td>
						</tr>
						<tr>
							<th>基本信息</th>
							<td>200间客房</td>
						</tr>
						<tr>
							<th class="jdxq_jdjj">酒店简介</th>
							<td>
								酒店位于市中心王府井地区，周边景点众多，靠近天安门广场、故宫，毗邻王府井步行街、新东方广场仅150米。<br />
								酒店位置优越，交通便捷。<br />
								酒店周围汇聚着北京最繁荣的购物、休闲娱乐、餐饮，酒店门口是东华门夜市汇聚各类特色小吃。<br />
								无论您是去天安门广场看升旗还是去故宫博物院，皇城根遗址公园。<br />
								酒店房间装修温馨典雅，基础生活设施齐备，将是您理想的温馨的住所，酒店全体员工期待您的光临。
							</td>
						</tr>
						<tr>
							<th class="jdxq_jdjj">入离时间</th>
							<td>
								入住时间：入住当天14:00后<br />
								离店时间：离店当天12:00前
							</td>
						</tr>
						<tr>
							<th>儿童政策</th>
							<td>每间客房最多容纳1名0至12岁儿童与成人共用现有床铺</td>
						</tr>
						<tr>
							<th>宠物</th>
							<td>不可携带宠物。</td>
						</tr>
						<tr>
							<th>通用设施</th>
							<td>
								<span style="color: #27C8E3;">√</span>电梯&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>空调&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>24小时热水&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>吹风机&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>门禁系统&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>净水机&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>自动售货机
							</td>
						</tr>
						<tr>
							<th>清洁服务</th>
							<td>
								<span style="color: #27C8E3;">√</span>洗衣服务&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>干衣机&nbsp;&nbsp;&nbsp;
								<span style="color: #27C8E3;">√</span>洗衣房&nbsp;&nbsp;&nbsp;
							</td>
						</tr>
					</table>
					<div id="jdxq_dt">
						<div class="ditu" id="map"></div>
					</div>
				</div>
			</div>
			
			<!--评价-->
			<a href="" name="pingjia"></a>
			<div id="pj">
				<div id="pj_bt">
					<span class="pj_bt_dp pj_dp">点评</span><span class="pj_dp_sl" id="num" onclick="dianping()"> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<!--<span class="pj_bt_dp pj_wd">问答</span><span class="pj_wd_sl">(180)</span>-->
				</div>
				<div id="dp">
					<!--<div id="wydp">
						
					</div>-->
					<div id="dpsx">
<%--						<input type="radio" name="sx" checked/><span class="dpsx_qb">全部</span>--%>
						<span><a href="/eva/getshijian">全部</a></span>
						<span><a href="/eva/gethaoping">好评</a></span>
						<span><a href="/eva/getzhongping">中评</a></span>
						<span><a href="/eva/getchagping">差评</a></span>
						<span class="dp_rmpx"><a href="/eva/getmen">热门排序↓</a></span>
						<span class="dp_sjpx"><a href="/eva/getshijian">时间排序↓</a></span>
					</div>


					<div id="yh1dp">
							<c:forEach items="${page.list}" var="evatype" varStatus="e">
								<table id="tab${e.count}" >
									<tr>
										<td  class="yhdp_yhmtp"><img src="${evatype.user.imageUrl}"/> </td>
										<td ><c:if test="${evatype.evaPing==1}">一颗星</c:if>
										<c:if test="${evatype.evaPing==2}">二颗星</c:if>
										<c:if test="${evatype.evaPing==3}">三颗星</c:if>
										<c:if test="${evatype.evaPing==4}">四颗星</c:if>
										<c:if test="${evatype.evaPing==5}">五颗星</c:if></td>
									</tr>

									<tr>
										<td align="center">${evatype.user.username}</td>
										<td>评论：${evatype.evaContent} ${evatype.kfId}${evatype.userId}</td>
									</tr>
									<c:forEach items="${evatype.evaReply}" var="reply" varStatus="s">
									<tr >
                                         <td></td>
										<td>回复：${reply.replyContent}  时间：${reply.replyTime}</td>
									</tr>
									</c:forEach>
									<tr>
										<td rowspan="2" class="yhdp_yhm"><span></span></td>
										<td class="yhdp_img">
											<img src="${evatype.evaimg.evaImgPath}"/>
<%--											<img src="/img/002.jpg"/>--%>
<%--											<img src="/img/003.jpg"/>--%>
<%--											<img src="/img/004.jpg"/>--%>
										</td>
									</tr>
									<tr class="yhdp_zh">
										<td>
											<span>时间：${evatype.evaTime}</span>
											<span class="yhdp_yy"><span><img src="/img/dianzan.png" class="dianzan"/><img src="/img/dianzan2.png" class="dianzan2"/>
											</span>&nbsp;<a href="/eva/dingzhua?uid=${evatype.userId}&&kfid=${evatype.kfId}" >有用</a> </span>
											<span class="yhdp_pl"><span><img src="/img/xinxi.png" class="xinxi"/><img src="/img/xinxi2.png" class="xinxi2"/>
<%--											</span>&nbsp;<a href="/eva/evareply" onclick="hpjian()">评论</a> </span>--%>
											</span><a onclick="hpjian(${e.count})">评论</a> </span>
											<input type="hidden" id="num${e.count}" value="1">
											<input type="hidden" id="kf${e.count}" value="${evatype.kfId}">
										</td>
									</tr>
								</table>
							</c:forEach>
					</div>

						</table>
					</div>
					<div id="yhdp_fenye">
						<a href="${path}/eva/getPage?pageNo=1">首页</a>
						<a href="${path}/eva/getPage?pageNo=${page.isFirstPage?1:page.prePage}">上一页</a>
						<c:forEach begin="1" end="${page.pages}" varStatus="i">
							<a href="${path}/eva/getPage?pageNow=${i.count}">${i.count}</a>
						</c:forEach>
						<a href="${path}/eva/getPage?pageNo=${page.isLastPage?page.pages:page.nextPage}">下一页</a>
						<a href="${path}/eva/getPage?pageNo=${page.nextPage}">尾页</a>
						<span>${page.pageNum}/${page.pages}页</span>
						<span>共${page.total}条</span>
					</div>
				</div>
			</div>
			
			<!--页脚-->
			<div id="" style="margin-top: 100px;">
				<img src="img/yejiao.png"/>
			</div>
		</div>
	</body>
<script type="text/javascript">

	var pj="${pingjia}";
	if(pj=="1"){
		location.href="#pingjia";
	}
	<%--var jd="${jiudian}";--%>
	<%--if(jd=="2"){--%>
	<%--	location.href="#fangjianyuding";--%>
	<%--}--%>

	var pwie= "${dingwie}";
	// alert(pwie);
	if(pwie=="10"){
		location.href="#shouyebufen";
	}else if(pwie=="11"){
		location.href="#fangjianyuding";
	}else if(pwie=="12"){
		location.href="#jiudianxiangqing";
	}else if(pwie=="13"){
		location.href="#pingjia";
	}

   function hpjian(id){
   		var numId=$("#num"+id).val();
	   var kfid=$("#kf"+id).val();
	   // alert(kfid);
   		if(numId==1){
			var tabObj=$("#tab"+id);
			var trObj = $("<tr>");
			trObj.attr("id","tr"+id);
			var tdObj1 = $("<td>");
			var tdObj2 = $("<td>");
			var textareaObj = $("<textarea>");
			var inpObj=$("<input>");
			textareaObj.attr("cols","120");
			textareaObj.attr("rows","10");
			textareaObj.attr("id","area"+id);
			inpObj.attr("value","提交");
			inpObj.attr("type","button");
			inpObj.attr("onclick","sumTextArea("+id+","+kfid+")");
			tdObj2.append(textareaObj);
			tdObj2.append(inpObj);
			trObj.append(tdObj1);
			trObj.append(tdObj2);
			tabObj.append(trObj);
			$("#num"+id).val(2);
		}else if(numId==2){
   			$("#tr"+id).remove();
			$("#num"+id).val(1);
		}
   }

  function sumTextArea(id,kfid){
   	var ccc=$("#area"+id).val();
	  $.ajax({
		  url:"/eva/evareply?ccc="+ccc+"&&kfid="+kfid,
		  type:"POST",
		  dataType:"text",
		  success:function (data){
		  	// alert(data)
		  }
	  });
  }


	// function dianping(){
    //      $.ajax({
	// 		 url:"/eva/evacount",
	// 		 dataType:"json",
	// 		 type:"POST",
	// 		 success:function (data){
	// 		 	alert(data)
    //           document.getElementById("num").innerHTML=data;
	// 		 }
	// 	 })
	// }
</script>
</html>

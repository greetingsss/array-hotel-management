<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String path = request.getContextPath();
    request.setAttribute("path",path);%>
<html>
<head>
    <title>Title</title>
    <script src="${path}/js/echarts/echarts.min.js"></script>
    <script src="${path}/js/jquery.min.js"></script>
</head>
<body>
    <div id="main" style="width: 550px;height:350px;float: left"></div>
    <div id="gender" style="width: 550px;height:350px;float: left"></div>
    <div id="avg" style="width: 550px;height:350px;float: left"></div>
    <div id="order" style="width: 550px;height:350px;float: left"></div>
</body>
<script type="text/javascript">
    showMain();
    function  showMain(){
        var myDiv=document.getElementById('main');
        var echartsEle = echarts.init(myDiv,'dark');
        option = {
            title: {
                text: '小雨'
            },
            legend: {
                data: ['Allocated Budget', 'Actual Spending']
            },
            radar: {
                // shape: 'circle',
                indicator: [
                    { name: 'Sales', max: 6500 },
                    { name: 'Administration', max: 16000 },
                    { name: 'Information Technology', max: 30000 },
                    { name: 'Customer Support', max: 38000 },
                    { name: 'Development', max: 52000 },
                    { name: 'Marketing', max: 25000 }
                ]
            },
            series: [
                {
                    name: 'Budget vs spending',
                    type: 'radar',
                    data: [
                        {
                            value: [3000, 3000, 20000, 35000, 50000, 18000],
                            name: 'Allocated Budget'
                        },
                        {
                            value: [6500, 14000, 28000, 26000, 42000, 21000],
                            name: 'Actual Spending'
                        }
                    ]
                }
            ]
        };
        echartsEle.setOption(option);
    }

    showGeder();
    function showGeder(){
        var myDiv=document.getElementById('gender');
        var echartsEle = echarts.init(myDiv,'dark');
        $.post("/stat/getGender",function (data) {
            // var str = JSON.stringify(data);
            // alert(str);
            var option = {
                tooltip: {
                    trigger: 'item'
                },
                legend: {
                    top: '5%',
                    left: 'center'
                },
                series: [
                    {
                        name: 'Access From',
                        type: 'pie',
                        radius: ['40%', '70%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        label: {
                            show: false,
                            position: 'left'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: '20',
                                fontWeight: 'bold'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data:data
                    }
                ]
            };
            echartsEle.setOption(option);
        },"json");


    }

    showRoomAvg();
    function showRoomAvg(){
        // {name:"['大床房',总统房,情侣房]",price:"[100,200,300]"}
        var myDiv=document.getElementById('avg');
        var echartsEle = echarts.init(myDiv,'dark');
        $.post("/stat/getAvg",function (data){
            var nameData=data.name;
            var priceData=data.price;
            var option = {
                xAxis: {
                    type: 'category',
                    data: nameData
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        data: priceData,
                        type: 'bar'
                    }
                ]
            };
            echartsEle.setOption(option);
        },"json");
    }
    showorder();
   function showorder(){
       var myDiv=document.getElementById('order');
       var echartsEle = echarts.init(myDiv,'dark');
       $.post("/stat/getorder",function (data) {
           var money=data.money;
           var orderMonth=data.ordermonth;
           var option = {
           xAxis:{
               type: 'category',
                   data: orderMonth
                },
               yAxis: {
                   type: 'value'
               },
               series: [
                   {
                       data:money,
                       type: 'line',
                       smooth: true
                   }
               ]
           };
           echartsEle.setOption(option);
       },"json");
       }

</script>

</html >

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String path = request.getContextPath();
    request.setAttribute("path",path);%>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>酒店管理系统 - 个人评价</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
          rel="stylesheet">
    <link href="${path}/css/animate.css" rel="stylesheet">
    <link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>酒店评价</h5>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" id="commentForm" action="${path}/eva/addEva">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">评价内容</label>
                            <div class="col-sm-7">
                                <textarea name="evaContent" cols="80"  rows="10"></textarea>
                                <input type="hidden"  name="userId" value="${userinfo.id}"  >
                                <input type="hidden"  name="kfId" value="${kfId}"  >
                                <input type="hidden"  name="orderId" value="${orderId}"  >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">是否匿名</label>
                            <div class="col-sm-7">
                                <select name="isNm">
                                    <option value="1">匿名</option>
                                    <option value="0">不匿名</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">评价等级</label>
                            <div class="col-sm-7">
                                <select name="evaPing">
                                    <option value="1">一颗星</option>
                                    <option value="2">二颗星</option>
                                    <option value="3">三颗星</option>
                                    <option value="4">四颗星</option>
                                    <option value="5">五颗星</option>
                                </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">评价图片</label>
                            <div class="col-sm-7" id="mydiv">
                                <input type="file" class="form-control" id="txImgs"  onchange="fileUpload()" >
                                <img src="${path}/img/tjtp.jpg" id="myImg" width="100px" height="100px">
                                <input type="hidden" id="fileImg" name="imageUrl"  >
                            </div>

                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-8">
                                <button class="btn btn-success" type="submit">提交</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                <button class="btn btn-white" type="reset" >取&nbsp;&nbsp;消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>

<!-- Data Tables -->
<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<!-- 自定义js -->
<script src="${path}/js/content.js?v=1.0.0"></script>

<!-- 表单验证 -->
<script src="${path}/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${path}/js/plugins/validate/messages_zh.min.js"></script>

<!-- layer javascript -->
<script src="${path}/js/plugins/layer/layer.min.js"></script>
<script type="text/javascript" >
    fileUpload();
    function fileUpload(){
        var fileObj = $("#txImgs");
        var fileEle=fileObj[0].files[0];
        var formData = new FormData();
        formData.append("file",fileEle);
        $.ajax({
            type:"post",
            url:"/kfRoom/fileUpload",
            data:formData,
            dataType: "json",
            contentType:false,
            cache:false,
            processData:false,
            success:function (data){
                var url=data.fileName;
                // var imgEle = document.createElement("img");
                // imgEle.src=url;
                // imgEle.width=100;
                // imgEle.height=100;
                // var mydiv = document.getElementById("mydiv");
                // mydiv.appendChild(imgEle);
                // var inpEle = document.createElement("input");
                // inpEle.type=hidden;
                // inpEle.name='imageUrl';
                // inpEle.value=url;
                // mydiv.appendChild(inpEle)
                document.getElementById("myImg").src=url;
                document.getElementById("fileImg").value=url;
            }
        });
    }
</script>

</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="${path}/css/logins.css">
		<script src="${path}/js/js/jquery.min.js"></script>
		<script src="${path}/js/js/login.js"></script>

<%--		<script src="${path}/jsjs/jquery.min.js" type="text/javascript" charset="utf-8"></script>--%>
<%--		<script src="${path}js/js/login.js" type="text/javascript" charset="utf-8"></script>--%>
<%--		<link rel="${path}stylesheet" type="text/css" href="${path}css/login.css"/>--%>
	</head>
	<body>
		<div id="login">
			<div id="login_title">
				<span id="jdmc">王府井永光酒店</span>
				<a href="/eva/getPage"><img src="/img/fangzi.png" width="31px"/><span>首页</span></a>
			</div>
			<div id="img">
				<img src="/img/login001.jpg"/>
				<div id="img_content">
					<form action="${path}/user/updateyem" method="post">
						<table cellpadding="20px" >
							<tr>
								<td class="fistTr">用户名：</td>
								<td class="sTr"><input type="text"  name="username" value="${user.username}" readonly="readonly"/></td>
							</tr>
							<tr>
								<td class="fistTr">昵称：</td>
								<td class="sTr"><input type="text" name="nickname" value="${user.nickname}"/></td>
							</tr>
							<tr>
								<td class="fistTr">真实姓名：</td>
								<td class="sTr"><input type="text" name="realname" value="${user.realname}"/></td>
							</tr>
							<tr>
								<td class="fistTr">性别：</td>
								<td class="sTr">
									<input type="radio" value="1" name="gender" <c:if test="${user.gender==1}" >checked</c:if> ><i></i>男
									<input type="radio" value="0" name="gender" <c:if test="${user.gender==0}" >checked</c:if> > <i></i>女
								</td>
							</tr>
							<tr>
								<td class="fistTr">email：</td>
								<td class="sTr"><input type="text"  name="email" value="${user.email}" /></td>
							</tr>
							<tr>
								<td class="fistTr">电话号码：</td>
								<td class="sTr"><input type="text"  name="phone" value="${user.phone}" /></td>
							</tr>
							<tr>
								<td class="fistTr">身份证号：</td>
								<td class="sTr"><input type="text"  name="idCard" value="${user.idCard}" readonly="readonly"/></td>
							</tr>
							<tr>
								<td class="fistTr">头像：</td>
								<td class="sTr">
<%--									<input type="file" name="imageUrl" value="${user.imageUrl}"/>--%>
									<input type="file" name="imageUrl" id="imageUrl" onchange="fileupdate()">
									<img src="${user.imageUrl}" id="myImg" width="100px" height="100px">
									<input type="hidden" id="fileImg" name="fileImg">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="submit" value="修改" id="button_zc" />
									<input type="reset" value="清空" id="button_qk" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div id="login_foot">
				<img src="/img/yejiao2.png"/>
			</div>
		</div>
	</body>
<script type="text/javascript">
	function fileupdate(){
		var fileObj=$("#imageUrl");
		var fileEle=fileObj[0].files[0];
		var formData= new FormData();
		formData.append("file",fileEle);
		$.ajax({
			type:"post",
			url:"/user/userfile",
			data:formData,
			dataType: "json",
			contentType:false,
			cache:false,
			processData:false,
			success:function (data) {
				var url = data.fileName;
				document.getElementById("myImg").src = url;
				document.getElementById("fileImg").value = url;
			}
		});
	}
</script>
</html>

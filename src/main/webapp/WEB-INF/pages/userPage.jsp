<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>酒店管理系统 - 用户列表</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">
	<link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<!-- Data Tables -->
	<link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
		rel="stylesheet">
	<link href="${path}/css/animate.css" rel="stylesheet">
	<link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="${path}/dist/sweetalert.css">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>用户列表</h5>
					</div>
					<div class="ibox-content">
						<div style="margin-bottom: 8px;float: left">
                            <a href="${path}/user/addPreUser" class="btn btn-success">添加用户</a>
                            <a href="${path}/user/exportUser" class="btn btn-success">用户导出</a>
							<a  class="btn btn-success" onclick="importUser()">用户导入</a>
						</div>
						<div style="float:left">
							<input type="file" id="file"  >
						</div>
						<table class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>序号</th>
									<th>用户名</th>
									<th>昵称</th>
									<th>真实姓名</th>
									<th>性别</th>
									<th>头像</th>
									<th>邮箱地址</th>
									<th>电话号码</th>
									<th>身份证号</th>
									<th>创建时间</th>
									<th>状态</th>
									<th>操作人</th>
									<th>操作</th>
								</tr>
							</thead>

							<tbody>
							<tbody>
								<c:forEach items="${page.list}" var="user" varStatus="u">
									<tr class="gradeA">
										<td>${u.index+1}</td>
										<td>${user.username}</td>
										<td>${user.nickname}</td>
										<td>${user.realname}</td>
										<td>
											<c:if test="${user.gender ==0}">女</c:if>
											<c:if test="${user.gender ==1}">男</c:if>
										</td>
										<td>${user.imageUrl}</td>
										<td>${user.email}</td>
										<td>${user.phone}</td>
										<td>${user.idCard}</td>
										<td>${user.createTime}</td>
										<td>
											<c:if test="${user.status ==1}">启用</c:if>
											<c:if test="${user.status ==2}">禁用</c:if>
											<c:if test="${user.status ==0}">删除</c:if>
										</td>
										<td>${user.opratorId}</td>
										<td>
											<a href="##" onclick="distRole(${user.id})" class="btn btn-primary">分配角色</a>&nbsp;&nbsp;
											<a href="${path}/user/updPreUser?id=${user.id}" class="btn btn-primary">修改</a>&nbsp;&nbsp;
											<a href="${path}/user/delUser?id=${user.id}" class="btn btn-danger">删除</a></td>
									</tr>
								</c:forEach>
							</tbody>

						</table>
						<div>
							<span style="float: left; padding: 5px">
								当前&nbsp;<span style="color: red;">${page.pageNum}</span>&nbsp;/&nbsp;<b>${page.pages}</b>&nbsp;页&nbsp;&nbsp;
								总共&nbsp;<b>${page.total}</b>&nbsp;条</span>
							<nav aria-label="Page navigation" style="margin: 0 auto; width: 240px">
								<ul class="pagination" style="margin: 0;">
									<li>
										<a href="${path}/user/userPage?pageNo=${page.isFirstPage?1:page.prePage}"
										   aria-label="Previous"> <span aria-hidden="true">前一页</span>
										</a>
									</li>
									<c:forEach  begin="1" end="${page.pages}" varStatus="i">
											<li><a href="${path}/user/userPage?pageNo=${i.count}">${i.count}</a></li>
									</c:forEach>
									<li><a href="${path}/user/userPage?pageNo=${page.isLastPage?page.pages:page.nextPage}"
										   aria-label="Next"> <span aria-hidden="true">后一页</span>
									</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 全局js -->
	<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>

	<!-- Data Tables -->
	<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- 自定义js -->
	<script src="${path}/js/content.js?v=1.0.0"></script>

 	<!-- layer javascript -->
    <script src="${path}/js/plugins/layer/layer.min.js"></script>
    
</body>
<script type="text/javascript">
	function distRole(userId){
		layer.open({
			type:2,
			content:'/user/toDistRole?userId='+userId,
			area :['800px','800px'],
		});
	}

	function importUser(){
		var fileObj = $("#file");
		var fileEle=fileObj[0].files[0];
		var formData = new FormData();
		formData.append("file",fileEle);
		$.ajax({
			type:"post",
			url:"/user/importUser",
			data:formData,
			dataType: "text",
			contentType:false,
			cache:false,
			processData:false,
			success:function (data){
				if(data=="success"){
					alert("导入成功");
					$("#file").val("");
					location.href="/user/userPage";
				}

			}
		});
	}
</script>
</html>

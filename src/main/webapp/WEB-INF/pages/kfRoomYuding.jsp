<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>酒店管理系统 - 客房信息列表</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">
	<link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<!-- Data Tables -->
	<link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
		rel="stylesheet">
	<link href="${path}/css/animate.css" rel="stylesheet">
	<link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="${path}/dist/sweetalert.css">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>客房信息列表</h5>
					</div>
					<div class="ibox-content">
						<div style="margin-bottom: 8px">
                            <a href="${path}/kfRoom/addPrekfRoom" class="btn btn-success">空闲客房信息</a>
							<a href="${path}/user/jump" class="btn btn-primary">退出</a>&nbsp;&nbsp;
						</div>
						<table class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>序号</th>
									<th>房间图片</th>
									<th>房间类型</th>
									<th>客房面积</th>
									<th>客房简介</th>
									<th>客房单价</th>
									<th>客房活动价格</th>
									<th>房间号</th>
									<th>预定时间</th>
									<th>结束时间</th>
									<th>天数</th>
									<th>支付金额</th>
									<th>操作</th>
								</tr>
							</thead>

							<tbody>
							<tbody>
								<c:forEach items="${page.list}" var="kfRoom" varStatus="k">
									<tr class="gradeA">
										<td>${k.count}</td>
										<td><img src="${kfRoom.kfImg.kfImgPath}" width="30px" height="30px"></td>
										<td>${kfRoom.kfType.typeName}</td>
										<td>${kfRoom.kfArea}</td>
										<td>${kfRoom.kfDesc}</td>
										<td>${kfRoom.kfPrice}</td>
										<td ><input id="dan" value="${kfRoom.kfHdPrice}"> </td>
										<td>
											${kfRoom.tbRoom.roomNum}
<%--												${kfRoom.tbRoom.roomStatus}--%>
<%--											        <c:if test="${kfRoom.tbRoom.roomStatus==0}"> 可以预定</c:if>--%>
<%--													<c:if test="${kfRoom.tbRoom.roomStatus==1}"> 无法预定</c:if>--%>
<%--													<c:if test="${kfRoom.tbRoom.roomStatus==2}"> 以入住</c:if>--%>
										</td>
										<td><input type="date" id="yuding" name="orderYdTime" ></td>
										<td><input type="date" id="jiesu" onchange="jies()" name="orderRzTime"></td>
										<td ><input type="text" id="days" name="orderDay" readOnly>
										<td ><input type="text" id="money" name="orderPatMoney" readOnly>
										</td>
										<td>
<%--											<a href="${path}/kfRoom/delKfRoom?id=${kfRoom.id}" class="btn btn-primary">添加图片</a>--%>
<%--											<a href="${path}/order/yuding?kfid=${kfRoom.id}&&star=${orderYdTime}" class="btn btn-primary">预定</a>&nbsp;&nbsp;--%>
											<input onclick="ding(kfid=${kfRoom.id})" value="预定" class="btn-primary">

										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div>
							<span style="float: left; padding: 5px">
								当前&nbsp;<span style="color: red;">${page.pageNum}</span>&nbsp;/&nbsp;<b>${page.pages}</b>&nbsp;页&nbsp;&nbsp;
								总共&nbsp;<b>${page.total}</b>&nbsp;条</span>
							<nav aria-label="Page navigation" style="margin: 0 auto; width: 240px">
								<ul class="pagination" style="margin: 0;">
									<li>
										<a href="${path}/kfRoom/roomding?pageNo=${page.isFirstPage?1:page.prePage}"
										   aria-label="Previous"> <span aria-hidden="true">前一页</span>
										</a>
									</li>
									<c:forEach  begin="1" end="${page.pages}" varStatus="i">
											<li><a href="${path}/kfRoom/roomding?pageNo=${i.count}">${i.count}</a></li>
									</c:forEach>
									<li><a href="${path}/kfRoom/roomding?pageNo=${page.isLastPage?page.pages:page.nextPage}"
										   aria-label="Next"> <span aria-hidden="true">后一页</span>
									</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 全局js -->
	<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>

	<!-- Data Tables -->
	<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- 自定义js -->
	<script src="${path}/js/content.js?v=1.0.0"></script>

 	<!-- layer javascript -->
    <script src="${path}/js/plugins/layer/layer.min.js"></script>
  <script>
	  function jies(){
	  	var jie=document.getElementById('jiesu').value;
	  	var yu=document.getElementById('yuding').value;
	  	if(yu<jie){
	  	var start=new Date(yu);
	    var end=new Date(jie);
		var day=(end.getTime()-start.getTime())/(1000*3600*24);
		 document.getElementById('days').value=day;
			var money= document.getElementById('dan').value*day;
			document.getElementById('money').value=money;
		}else {
			alert("结束时间不能小于开始时间")
			document.getElementById('yuding').value='';
			document.getElementById('jiesu').value='';
		}
	  }

	  function ding(kfid){
		  var str=  document.getElementById('yuding').value;
		  var end=document.getElementById('jiesu').value;
		  var day=document.getElementById('days').value;
		  var money=document.getElementById('money').value;
		  $.ajax({
			  url:"/order/yuding?kfId="+kfid+"&orderYdTime="+str+"&orderRzTime="+end+"&orderPayMoney="+money+"&orderDay="+day,
			  type:"POST",
			  dataType:"text",
			  success:function (data){
				  if(data=="1"){
				  	alert("预定成功")
				  }
			  }
		  })
	    	<%--window.location.href="${path}/order/yuding?kfId="+kfid+"&orderYdTime="+str+"&orderRzTime="+end+"&orderPayMoney="+money;--%>
	  }
  </script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>酒店管理系统 - 修改客房信息</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">
	<link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">

	<!-- Data Tables -->
	<link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
		rel="stylesheet">
	<link href="${path}/css/animate.css" rel="stylesheet">
	<link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">

</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>修改客房信息</h5>
					</div>
					<div class="ibox-content">
						<form method="post" class="form-horizontal" id="commentForm" action="${path}/kfRoom/updKfRoom">
							<div class="form-group">
								<label class="col-sm-3 control-label">客房面积</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="kfArea" value="${kfRoom.kfArea}" >
									<input type="hidden" name="id" value="${kfRoom.id}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房简介</label>
								<div class="col-sm-7">
									<%--<input type="text" class="form-control" name="kfArea" >--%>
									<textarea name="kfDesc" cols="80" rows="20">${kfRoom.kfDesc}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房单价</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="kfPrice" value="${kfRoom.kfPrice}" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房活动价格</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="kfHdPrice" value="${kfRoom.kfHdPrice}" readonly >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房类别</label>
								<div class="col-sm-7">
									<select name="kfTypeId">
										<c:forEach items="${listType}" var="type">
											<option value="${type.id}" <c:if test="${kfRoom.kfTypeId ==type.id}">selected</c:if> >${type.typeName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">参与活动</label>
								<div class="col-sm-7">
									<select name="hdId">
										<option value="0">不参与活动</option>
										<%--<c:forEach items="${listHd}" var="hd">
											<option value="${hd.id}">${hd.name}</option>
										</c:forEach>--%>
									</select>
								</div>
							</div>

							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-8">
									<button class="btn btn-success" type="submit">修&nbsp;&nbsp;&nbsp;&nbsp;改</button>&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-white" type="reset" >取&nbsp;&nbsp;消</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 全局js -->
	<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>
	
	<!-- Data Tables -->
	<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- 自定义js -->
	<script src="${path}/js/content.js?v=1.0.0"></script>
	
	<!-- 表单验证 -->
	<script src="${path}/js/plugins/validate/jquery.validate.min.js"></script>
	<script src="${path}/js/plugins/validate/messages_zh.min.js"></script>
	
	<!-- layer javascript -->
    <script src="${path}/js/plugins/layer/layer.min.js"></script>
	<script>
	$().ready(function() {
	    $("#commentForm").validate();
	});
	$.validator.setDefaults({
	    submitHandler: function() {
	    	parent.layer.alert('添加成功！',{icon: 1}),
	    	form.submit();
	    }
	});
	</script>
</body>
</html>

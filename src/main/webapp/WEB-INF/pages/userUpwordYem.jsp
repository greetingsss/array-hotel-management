<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="${path}/css/logins.css">
		<script src="${path}/js/js/jquery.min.js"></script>
		<script src="${path}/js/js/login.js"></script>

<%--		<script src="${path}/jsjs/jquery.min.js" type="text/javascript" charset="utf-8"></script>--%>
<%--		<script src="${path}js/js/login.js" type="text/javascript" charset="utf-8"></script>--%>
<%--		<link rel="${path}stylesheet" type="text/css" href="${path}css/login.css"/>--%>
	</head>
	<body>
		<div id="login">
			<div id="login_title">
				<span id="jdmc">王府井永光酒店</span>
				<a href="eva/getPage"><img src="/img/fangzi.png" width="31px"/><span>首页</span></a>
			</div>
			<div id="img">
				<img src="/img/login001.jpg"/>
				<div id="img_content">
					<form action="${path}/user/updatepass" method="post">
						<table cellpadding="20px">
							<tr>
								<td class="fistTr">用户名：</td>
								<td class="sTr"><input type="text"  name="username" value="${user.username}" readonly="readonly"/></td>
							</tr>
							<tr>
								<td class="fistTr">密码：</td>
								<td class="sTr"><input type="password" name="password" value="${user.password}"/></td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="submit" value="修改" id="button_zc" />
									<input type="reset" value="清空" id="button_qk" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div id="login_foot">
				<img src="/img/yejiao2.png"/>
			</div>
		</div>
	</body>
</html>

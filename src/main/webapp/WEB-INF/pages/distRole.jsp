<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String path = request.getContextPath();
    request.setAttribute("path",path);%>
<html>
<head>
    <title>Title</title>
</head>
<style type="text/css">
    .left{
        float: left;
        width: 300px;
        height: 500px;
        /*border: 1px red solid;*/
    }
    .right{
        float: right;
        width: 300px;
        height: 500px;
        /*border: 1px red solid;*/
    }

    .lpo{
        position: absolute;
        left: 350px;
        top: 200px;
    }
    .rpo{
        position: absolute;
        left: 350px;
        top: 250px;
    }
    .mouse:hover{
        color: red;
    }
    .titSpan{
        font-size: 22px;
        font-weight: bold;
    }
    .borSty{
        border-style: inset;
        height: 650px;
    }
</style>
<body>
    <div class="left"  >
        <span class="titSpan">未分配角色</span>
        <div class="borSty" id="leftDiv">
            <c:forEach items="${unDistRole}" var="role">
                <div class="mouse" onclick="xuanzhong(this)">
                        ${role.roleName}
                    <input type="hidden" name="roleId" value="${role.id}">
                </div>
            </c:forEach>
            <%--<div class="mouse" onclick="xuanzhong(this)">系统管理员</div>
            <div class="mouse" onclick="xuanzhong(this)">预定管理员</div>
            <div class="mouse" onclick="xuanzhong(this)">客户</div>--%>
        </div>

    </div>
    <span>
        <span class="lpo"><img src="${path}/img/rjt.png" width="50px" onclick="rTol()" > </span>
        <span class="rpo" ><img src="${path}/img/zjt.png" width="50px" onclick="lTor()" > </span>
    </span>
    <div class="right" >
        <span class="titSpan">已分配角色</span>
        <form id="myform" action="${path}/user/addUserRole" method="post">
            <input type="hidden" name="userId" value="${userId}">
            <div class="borSty" id="rightDiv">
                <c:forEach items="${distRole}" var="role">
                    <div class="mouse" onclick="xuanzhong(this)">
                            ${role.roleName}
                        <input type="hidden" name="roleId" value="${role.id}">
                    </div>
                </c:forEach>
            </div>
            <input type="button" value="提交" onclick="sub()">
        </form>
    </div>
</body>
<script src="${path}/js/plugins/layer/layer.min.js"></script>
<script type="text/javascript">
    var o ;
    var type=true;
    function xuanzhong(obj){
        obj.style.color="blue";
        o=obj;
    }

    function lTor(){
        var rigtEle = document.getElementById("rightDiv");
        rigtEle.appendChild(o);
        o.style.color="black";
    }

    function rTol(){
        var rigtEle = document.getElementById("leftDiv");
        rigtEle.appendChild(o);
        o.style.color="black";
    }

    function sub(){
        var myform = document.getElementById("myform");
        myform.submit();
    }

</script>
</html>

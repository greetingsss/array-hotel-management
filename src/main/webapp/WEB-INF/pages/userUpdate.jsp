<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>酒店管理系统 - 修改用户</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">
	<link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">

	<!-- Data Tables -->
	<link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
		  rel="stylesheet">
	<link href="${path}/css/animate.css" rel="stylesheet">
	<link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">

</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>修改用户</h5>
				</div>
				<div class="ibox-content">
					<form method="post" class="form-horizontal" id="commentForm" action="${path}/user/updUser">
						<div class="form-group">
							<label class="col-sm-3 control-label">用户名</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="username" value="${user.username}" >
								<input type="hidden" name="id" value="${user.id}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">密码</label>
							<div class="col-sm-7">
								<input type="password" class="form-control" id="password"  name="password" value="${user.password}" minlength="6" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">昵称</label>
							<div class="col-sm-7">
								<input type="text" class="form-control"  name="nickname" value="${user.nickname}" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">真实姓名</label>
							<div class="col-sm-7">
								<input type="text" class="form-control"  name="realname" value="${user.realname}" >
							</div>
						</div>
						<%--<div class="form-group">
                            <label class="col-sm-3 control-label">确认密码</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control" placeholder="" name="password2" equalTo="#password" minlength="4" required>
                            </div>
                        </div>--%>
						<div class="form-group">
							<label class="col-sm-3 control-label">性别</label>
							<div class="col-sm-7">
								<div class="radio i-checks">
									<label>
										<input type="radio" value="1" name="gender" <c:if test="${user.gender==1}" >checked</c:if> > <i></i>男</label>
									<i style="margin-left: 25px"></i>
									<label>
										<input type="radio" value="0" name="gender"   <c:if test="${user.gender==0}" >checked</c:if>> <i></i>女</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">邮箱地址</label>
							<div class="col-sm-7">
								<input type="email" class="form-control"  name="email" value="${user.email}" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">电话号码</label>
							<div class="col-sm-7">
								<input type="text" class="form-control"  name="phone" value="${user.phone}" >
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">身份证号</label>
							<div class="col-sm-7">
								<input type="text" class="form-control"  name="idCard" value="${user.idCard}" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">状态</label>
							<div class="col-sm-7">
								<select class="form-control m-b" name="status">
									<option value="1" <c:if test="${user.status==1}">selected </c:if> >启用</option>
									<option value="2" <c:if test="${user.status==2}">selected </c:if> >禁用</option>
									<option value="0" <c:if test="${user.status==0}">selected </c:if>>删除</option>
								</select>
							</div>
						</div>
				</div>

				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<div class="col-sm-4 col-sm-offset-8">
						<button class="btn btn-success" type="submit">修&nbsp;&nbsp;改</button>&nbsp;&nbsp;&nbsp;&nbsp;
						<button class="btn btn-white" type="reset" >取&nbsp;&nbsp;消</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>

<!-- 全局js -->
<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>

<!-- Data Tables -->
<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<!-- 自定义js -->
<script src="${path}/js/content.js?v=1.0.0"></script>

<!-- 表单验证 -->
<script src="${path}/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${path}/js/plugins/validate/messages_zh.min.js"></script>

<!-- layer javascript -->
<script src="${path}/js/plugins/layer/layer.min.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate();
	});
	$.validator.setDefaults({
		submitHandler: function() {
			parent.layer.alert('修改成功！',{icon: 1}),
			form.submit();
		}
	});
</script>
</body>
</html>

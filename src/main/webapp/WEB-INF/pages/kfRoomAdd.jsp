<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>酒店管理系统 - 添加客房信息</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">
	<link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">

	<!-- Data Tables -->
	<link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
		rel="stylesheet">
	<link href="${path}/css/animate.css" rel="stylesheet">
	<link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">

</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>添加客房信息</h5>
					</div>
					<div class="ibox-content">
						<form method="post" class="form-horizontal"  id="commentForm" action="${path}/kfRoom/addkfRoom">
							<div class="form-group">
								<label class="col-sm-3 control-label">客房面积</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" name="kfArea" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房简介</label>
								<div class="col-sm-7">
									<%--<input type="text" class="form-control" name="kfArea" >--%>
									<textarea name="kfDesc" cols="80" rows="20"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房单价</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="kfPrice" name="kfPrice" onchange="setkfHdPrice()">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房活动价格</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="kfHdPrice" name="kfHdPrice" value="0" readonly >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">客房类别</label>
								<div class="col-sm-7">
									<select name="kfTypeId">
										<c:forEach items="${listType}" var="type">
											<option value="${type.id}">${type.typeName}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">参与活动</label>
								<div class="col-sm-7">
									<select id="hdSelect" name="hdId" onchange="setkfHdPrice()">
										<option value="0">不参与活动</option>
										<c:forEach items="${listHd}" var="hd">
											<option value="${hd.id}">${hd.hdDesc}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">图片上传</label>
								<div class="col-sm-7" id="mydiv">
									<input type="file" class="form-control" id="kfImgs"  name="kfImgs" onchange="fileUpload()" >
									<img src="${path}/img/hsxr.png" id="myImg" width="100px" height="100px">
									<input type="hidden" id="fileImg" name="fileImg" >
								</div>

							</div>

							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-8">
									<button class="btn btn-success" type="submit">添&nbsp;&nbsp;加</button>&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-white" type="reset" >取&nbsp;&nbsp;消</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 全局js -->
	<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>
	
	<!-- Data Tables -->
	<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- 自定义js -->
	<script src="${path}/js/content.js?v=1.0.0"></script>
	
	<!-- 表单验证 -->
	<script src="${path}/js/plugins/validate/jquery.validate.min.js"></script>
	<script src="${path}/js/plugins/validate/messages_zh.min.js"></script>
	
	<!-- layer javascript -->
    <script src="${path}/js/plugins/layer/layer.min.js"></script>
	<script type="text/javascript" >
	$().ready(function() {
	    $("#commentForm").validate();
	});
	$.validator.setDefaults({
	    submitHandler: function() {
	    	parent.layer.alert('添加成功！',{icon: 1}),
	    	form.submit();
	    }
	});

	function setkfHdPrice(){
		var selectId=$("#hdSelect").val();
		if(selectId==0){
			$("#kfHdPrice").val($("#kfPrice").val());
		}
		$.ajax({
			url:"/hd/getHdById",
			data:"id="+selectId,
			dataType:"json",
			success:function(data){
				//活动类型
				var type = data.hdType;
				//活动优惠
				var yh =data.hdYh;
				//客房单价
				var kfPriceValue = $("#kfPrice").val();
				if(type==0){//减免
					if(kfPriceValue!=undefined && kfPriceValue!="" && kfPriceValue!=null){
						var hdPrice=kfPriceValue-yh;
						if(hdPrice<=0){
							hdPrice=0;
						}
						$("#kfHdPrice").val(hdPrice);
					}else{
						alert("请先填写客房单价");
					}
				}else if(type==1){//折扣
					if(kfPriceValue!=undefined && kfPriceValue!="" && kfPriceValue!=null){
						var hdPrice=kfPriceValue*yh;
						$("#kfHdPrice").val(hdPrice);
					}else{
						alert("请先填写客房单价");
					}
				}
			}
		});
	}

	function fileUpload(){
		var fileObj = $("#kfImgs");
		var fileEle=fileObj[0].files[0];
		var formData = new FormData();
		formData.append("file",fileEle);
		alert(fileEle);
		$.ajax({
			type:"post",
			url:"/kfRoom/fileUpload",
			data:formData,
			dataType: "json",
			contentType:false,
			cache:false,
			processData:false,
			success:function (data){
				var url=data.fileName;
				// var imgEle = document.createElement("img");
				// imgEle.src=url;
				// var mydiv = document.getElementById("mydiv");
				// mydiv.appendChild(imgEle);
				document.getElementById("myImg").src=url;
				document.getElementById("fileImg").value=url;
			}
		});
	}

	</script>
</body>
</html>

<%--
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
      rel="stylesheet">
<link href="${path}/css/animate.css" rel="stylesheet">
<link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">
<html>
<head>
    <title>Title</title>
</head>
<body>
    <table width="300px">
        <tr>
            <td>
                <img src="" id="dayIcon">
               &lt;%&ndash; <span id="dayWeather"></span>
                <span id="dayTemp"></span>℃
                风力:<span id="dayWind"></span>&ndash;%&gt;
            </td>
            <td>
                <img src="" id="nightIcon">
             &lt;%&ndash;   <span id="nightWeather"></span>
                <span id="nightTemp"></span>℃
                风力:<span id="nightWind"></span>&ndash;%&gt;
            </td>
            <td>
                <img src="" id="icon">
             &lt;%&ndash;   <span id="weather"></span>
                <span id="temp"></span>℃
                风力:<span id="wind"></span>
                湿度:<span id="humidity"></span>
                pm2.5:<span id="pm25"></span>&ndash;%&gt;
            </td>
        </tr>
    </table>
</body>
<!-- 全局js -->
<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>

<!-- Data Tables -->
<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<!-- 自定义js -->
<script src="${path}/js/content.js?v=1.0.0"></script>

<!-- layer javascript -->
<script src="${path}/js/plugins/layer/layer.min.js"></script>
<script type="text/javascript">

    getWeather();
    function getWeather(){
        $.ajax({
            url:"/wether/getWeaher",
            data:"code=500237",
            dataType:"json",
            success:function (data){
                $("#dayIcon").attr("src",data.data.dayIcon);
                $("#nightIcon").attr("src",data.data.nightIcon);
                $("#icon").attr("src",data.data.Icon);
                $("#dayWeather").text(data.data.dayWeather);
                $("#nightWeather").text(data.data.nightWeather);
                $("#weather").text(data.data.weather);
                $("#dayTemp").text(data.data.dayTemp);
                $("#temp").text(data.data.temp);
                $("#nightTemp").text(data.data.nightTemp);
                $("#dayWind").text(data.data.dayWind);
                $("#nightWind").text(data.data.nightWind);
                $("#wind").text(data.data.wind);
                $("#humidity").text(data.data.humidity);
                $("#pm25").text(data.data.pm25);
            }
        });

    }
</script>
</html>
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
    request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico"> <link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="${path}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${path}/css/animate.css" rel="stylesheet">
    <link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">
    <script>
        var myVar=setInterval(function(){myTimer()},1000);
        function myTimer(){
            var d=new Date();
            var t=d.toLocaleTimeString();
            document.getElementById("time").innerHTML=t;
        }
    </script>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="jumbotron">
                <div style="margin-left: 250px">
                    <p>&nbsp;</p>

                    <h1>${sessionScope.loginInfo.name}</h1>
                    <p>&nbsp;</p>
                    <p>欢迎登录酒店管理系统</p>
                    <p>身处四季,梦回故里;人在厢房,四季飘香。</p>
                    <p id="time">&nbsp;</p>
                    <p>&nbsp;</p>
                    <div>
                        <img src="/img/1.gif" width="120px">&nbsp;&nbsp;

                        <img src="/img/1.gif" width="120px">&nbsp;&nbsp;

                        <img src="/img/2.gif" width="120px">&nbsp;&nbsp;

                        <img src="/img/5.gif" width="120px">&nbsp;&nbsp;

                        <img src="/img/4.gif" width="120px">&nbsp;&nbsp;

                        <img src="/img/3.gif" width="120px">
                    </div>
                    <div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;吴睿&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;宋思远&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;庄梦臣&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;
                        &nbsp;陈苗挺&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;秦海山&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        &nbsp;徐嵘森&nbsp;

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 全局js -->
<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="${path}/js/content.js?v=1.0.0"></script>

<!-- layer javascript -->
<script src="${path}/js/plugins/layer/layer.min.js"></script>
<script>
    $("a").click(function () {
        parent.layer.alert('签到成功！')
    });
</script>
</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>酒店管理系统 - 菜单列表</title>
	<meta name="keywords" content="">
	<meta name="description" content="">

	<link rel="shortcut icon" href="favicon.ico">
	<link href="${path}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
	<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<!-- Data Tables -->
	<link href="${path}/css/plugins/dataTables/dataTables.bootstrap.css"
		rel="stylesheet">
	<link href="${path}/css/animate.css" rel="stylesheet">
	<link href="${path}/css/style.css?v=4.1.0" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="${path}/dist/sweetalert.css">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>菜单列表</h5>
					</div>
					<div class="ibox-content">
						<div style="margin-bottom: 8px">
                            <a href="${path}/menu/addPreMenu" class="btn btn-success">添加菜单</a>
						</div>
						<table class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>序号</th>
									<th>菜单名称</th>
									<th>菜单地址</th>
									<th>菜单路径</th>
									<th>菜单优先级</th>
									<th>菜单描述</th>
									<th>菜单图标</th>
									<th>父菜单</th>
									<th>操作</th>
								</tr>
							</thead>

							<tbody>
							<tbody>
								<c:forEach items="${page.list}" var="menu" varStatus="m">
									<tr class="gradeA">
										<td>${m.count}</td>
										<td>${menu.menuName}</td>
										<td>${menu.menuUrl}</td>
										<td>${menu.menuPath}</td>
										<td>${menu.menuPro}</td>
										<td>${menu.menuDesc}</td>
										<td>${menu.menuIcon}</td>
										<td>${menu.pmenu.menuName}</td>
										<td>
											<a href="${path}/menu/updPreMenu?id=${menu.id}" class="btn btn-primary">修改</a>&nbsp;&nbsp;
											<a href="${path}/menu/delMenu?id=${menu.id}" class="btn btn-danger">删除</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div>
							<span style="float: left; padding: 5px">
								当前&nbsp;<span style="color: red;">${page.pageNum}</span>&nbsp;/&nbsp;<b>${page.pages}</b>&nbsp;页&nbsp;&nbsp;
								总共&nbsp;<b>${page.total}</b>&nbsp;条</span>
							<nav aria-label="Page navigation" style="margin: 0 auto; width: 240px">
								<ul class="pagination" style="margin: 0;">
									<li>
										<a href="${path}/menu/menuPage?pageNo=${page.isFirstPage?1:page.prePage}"
										   aria-label="Previous"> <span aria-hidden="true">前一页</span>
										</a>
									</li>
									<c:forEach  begin="1" end="${page.pages}" varStatus="i">
											<li><a href="${path}/menu/menuPage?pageNo=${i.count}">${i.count}</a></li>
									</c:forEach>
									<li><a href="${path}/menu/menuPage?pageNo=${page.isLastPage?page.pages:page.nextPage}"
										   aria-label="Next"> <span aria-hidden="true">后一页</span>
									</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 全局js -->
	<script src="${path}/js/jquery.min.js?v=2.1.4"></script>
	<script src="${path}/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="${path}/js/plugins/jeditable/jquery.jeditable.js"></script>

	<!-- Data Tables -->
	<script src="${path}/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="${path}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- 自定义js -->
	<script src="${path}/js/content.js?v=1.0.0"></script>

 	<!-- layer javascript -->
    <script src="${path}/js/plugins/layer/layer.min.js"></script>
    
</body>
</html>

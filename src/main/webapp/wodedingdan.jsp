<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<% String path = request.getContextPath();
    request.setAttribute("path", path);%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>我的订单</title>
    <link rel="stylesheet" href="${path}/css/wodedingdan.css">
    <script src="${path}/js/js/jquery.min.js"></script>
    <script src="${path}/js/js/wodedingdan.js"></script>
    <%--		<link rel="/stylesheet" type="text/css" href="${path}/css/wodedingdan.css"/>--%>
    <%--		<script src="${path}/js/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>--%>
    <%--		<script src="${path}/js/js/wodedingdan.js" type="text/javascript" charset="utf-8"></script>--%>
</head>
<body>
<div id="div">
    <!--头-->
    <div id="head">
        <div id="title">
            <span id="logo"><a href="index.jsp#shouyebufen">王府井永光酒店</a></span>
            <span class="dh"><a href="/eva/getPage?dingwie=1">首页</a></span>
            <span class="dh"><a href="/eva/getPage?dingwie=2">房间预定</a></span>
            <span class="dh"><a href="/eva/getPage?dingwie=3">酒店详情</a></span>
            <span class="dh"><a href="/eva/getPage?dingwie=4">评价</a></span>
            <span class="dh"><a href="/user/dingdan">我的订单</a></span>
            <span class="dh"><a href="/logout">退出</a></span>
        </div>
    </div>
    <div id="headTS"></div>

    <div id="content">
        <!--左边列表-->
        <div id="left">
            <ul>
                <li class="wdxxx">我的信息</li>
                <li class="ddxxx">订单信息</li>
                <!--<li class="wdxiaoxix">我的消息</li>-->
                <li class="zhaqx">账户安全</li>
            </ul>
        </div>

        <!--右边内容-->
        <div id="right">
            <!--我的信息-->
            <div id="wdxx">
                <div class="right_title">
                    我的信息
                </div>
                <div id="wdxx_tp">
                    <div><img src="${user.imageUrl}"></div>
                    <%--							<img src="/img/001.jpg" />--%>
                </div>
                <table border="0" cellpadding="20px">
                    <tr>
                        <td>用户名：</td>
                        <td>${user.username}</td>
                    </tr>
                    <tr>
                        <td>昵称：</td>
                        <td>${user.nickname}</td>
                    </tr>
                    <tr>
                        <td>真实姓名：</td>
                        <td>${user.realname}</td>
                    </tr>
                    <tr>
                        <td>性别：</td>
                        <td>
                            <c:if test="${user.gender ==0}">女</c:if>
                            <c:if test="${user.gender ==1}">男</c:if>
                        </td>
                    </tr>
                    <tr>
                        <td>email：</td>
                        <td>${user.email}</td>
                    </tr>
                    <tr>
                        <td>电话号码：</td>
                        <td>${user.phone}</td>
                    </tr>
                    <tr>
                        <td>身份证号码：</td>
                        <td>${user.idCard}</td>
                    </tr>
                </table>
            </div>

            <!--订单信息-->
            <%--					<a href="javascript:;" name="wang"></a>--%>
            <div id="ddxx">
                <div class="right_title">
                    订单信息
                </div>
                <div id="ddxx_title">
                    <span class="ddxxxq ddxx_qbdd">全部订单</span>
                    <span class="ddxxxq ddxx_jxz">预定</span>
                    <span class="ddxxxq ddxx_ycs">入住</span>
                    <span class="ddxxxq ddxx_ddp">待点评</span>
                </div>
                <%--						全部订单--%>
                <div class="ddxx_show ddxx_qbddx" id="quanbudingdan">
                    <c:forEach items="${page.list}" var="order" varStatus="i">
                        <div class="ddxx_xxxx">
                            <table cellpadding="10px">
                                <tr>
                                    <td rowspan="3" class="ddxx_mingcheng">王府井永光酒店</td>
                                    <td>${order.orderYdTime} —— ${order.orderRzTime}</td>
                                    <td class="ddxx_money">￥${order.orderPayMoney}</td>
                                </tr>
                                <tr>
                                    <td>观潮景区临江路3号</td>
                                        <%--											orderStatus--%>
                                    <td class="ddxx_jinxinzhong">
                                        <c:if test="${order.orderStatus==0}">预定</c:if>
                                        <c:if test="${order.orderStatus==1}">取消</c:if>
                                        <c:if test="${order.orderStatus==2}">入住</c:if>
                                        <c:if test="${order.orderStatus==3}">退房</c:if>
                                    </td>
                                    <td>
                                            <%--												<a href="##" onclick="pinglu()">评论</a>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>入住人数：2人</td>
                                    <!--<td><a href="" class="ddxx_dianp" >点评</a></td>-->
                                </tr>
                            </table>
                        </div>
                    </c:forEach>
                    <div id="ddxx_fenye">
                        <a href="${path}/user/dingdan?pageNo=1&typeDingDan=qbdd">首页</a>
                        <a href="${path}/user/dingdan?pageNo=${page.isFirstPage?1:page.prePage}&typeDingDan=qbdd">上一页</a>
                        <c:forEach begin="1" end="${page.pages}" varStatus="i">
                            <a href="${path}/user/dingdan?pageNo=${i.count}&typeDingDan=qbdd">${i.count}</a>
                        </c:forEach>
                        <a href="${path}/user/dingdan?pageNo=${page.isLastPage?page.pages:page.nextPage}&typeDingDan=qbdd">下一页</a>
                        <a href="${path}/user/dingdan?pageNo=${page.pages}&typeDingDan=qbdd">尾页</a>
                        <span>${page.pageNum}/${page.pages}页</span>
                        <span>共${page.total}条</span>
                    </div>

                </div>
                <%--预定订单--%>
                <div class="ddxx_show ddxx_qbddx" id="jingxingzhong">
                    <c:forEach items="${yuding.list}" var="order" varStatus="i">
                        <div class="ddxx_xxxx">
                            <table cellpadding="10px">
                                <tr>
                                    <td rowspan="3" class="ddxx_mingcheng">王府井永光酒店</td>
                                    <td>${order.orderYdTime} —— ${order.orderRzTime}</td>
                                    <td class="ddxx_money">￥${order.orderPayMoney}</td>
                                </tr>
                                <tr>
                                    <td>观潮景区临江路3号</td>

                                    <td class="ddxx_jinxinzhong">
                                        <c:if test="${order.orderStatus==0}">预定</c:if>
                                        <c:if test="${order.orderStatus==1}">取消</c:if>
                                        <c:if test="${order.orderStatus==2}">入住</c:if>
                                        <c:if test="${order.orderStatus==3}">退房</c:if>
                                    </td>
                                    <td>
                                            <%--												<a href="##" onclick="pinglu()">评论</a>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>入住人数：2人</td>
                                    <!--<td><a href="" class="ddxx_dianp" >点评</a></td>-->
                                </tr>
                            </table>
                        </div>
                    </c:forEach>
                    <div id="ddxx_fenye">
                        <a href="${path}/user/dingdan?pageNo=1&typeDingDan=yddd">首页</a>
                        <a href="${path}/user/dingdan?pageNo=${yuding.isFirstPage?1:yuding.prePage}&typeDingDan=yddd">上一页</a>
                        <c:forEach begin="1" end="${yuding.pages}" varStatus="i">
                            <a href="${path}/user/dingdan?pageNow=${i.count}&typeDingDan=yddd">${i.count}</a>
                        </c:forEach>
                        <a href="${path}/user/dingdan?pageNo=${yuding.isLastPage?yuding.pages:yuding.nextPage}&typeDingDan=yddd">下一页</a>
                        <a href="${path}/user/dingdan?pageNo=${yuding.pages}&typeDingDan=yddd">尾页</a>
                        <span>${yuding.pageNum}/${yuding.pages}页</span>
                        <span>共${yuding.total}条</span>
                    </div>
                </div>

                <%--入住--%>
                <div class="ddxx_show ddxx_qbddx" id="rzhu">
                    <c:forEach items="${zhu.list}" var="order" varStatus="i">
                        <div class="ddxx_xxxx">
                            <table cellpadding="10px">
                                <tr>
                                    <td rowspan="3" class="ddxx_mingcheng">王府井永光酒店</td>
                                    <td>${order.orderYdTime} —— ${order.orderRzTime}</td>
                                    <td class="ddxx_money">￥${order.orderPayMoney}</td>
                                </tr>
                                <tr>
                                    <td>观潮景区临江路3号</td>

                                    <td class="ddxx_jinxinzhong">
                                        <c:if test="${order.orderStatus==0}">预定</c:if>
                                        <c:if test="${order.orderStatus==1}">取消</c:if>
                                        <c:if test="${order.orderStatus==2}">入住</c:if>
                                        <c:if test="${order.orderStatus==3}">退房</c:if>
                                    </td>
                                    <td>
                                            <%--													<a href="##" onclick="pinglu()">评论</a>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>入住人数：2人</td>
                                    <!--<td><a href="" class="ddxx_dianp" >点评</a></td>-->
                                </tr>
                            </table>
                        </div>
                    </c:forEach>
                    <div id="ddxx_fenye">
                        <a href="${path}/user/dingdan?pageNo=1&typeDingDan=rz">首页</a>
                        <a href="${path}/user/dingdan?pageNo=${zhu.isFirstPage?1:zhu.prePage}&typeDingDan=rz">上一页</a>
                        <c:forEach begin="1" end="${zhu.pages}" varStatus="i">
                            <a href="${path}/user/dingdan?pageNow=${i.count}&typeDingDan=rz">${i.count}</a>
                        </c:forEach>
                        <a href="${path}/user/dingdan?pageNo=${zhu.isLastPage?zhu.pages:zhu.nextPage}&typeDingDan=rz">下一页</a>
                        <a href="${path}/user/dingdan?pageNo=${zhu.pages}&typeDingDan=rz">尾页</a>
                        <span>${zhu.pageNum}/${zhu.pages}页</span>
                        <span>共${zhu.total}条</span>
                    </div>
                </div>
                <%--待评论--%>
                <div class="ddxx_show ddxx_qbddx" id="tui">
                    <c:forEach items="${tui.list}" var="order" varStatus="i">
                        <div class="ddxx_xxxx">
                            <table cellpadding="10px">
                                <tr>
                                    <td rowspan="3" class="ddxx_mingcheng">王府井永光酒店</td>
                                    <td>${order.orderYdTime} —— ${order.orderRzTime}</td>
                                    <td class="ddxx_money">￥${order.orderPayMoney}</td>
                                </tr>
                                <tr>
                                    <td>观潮景区临江路3号</td>

                                    <td class="ddxx_jinxinzhong">
                                        <c:if test="${order.orderStatus==0}">预定</c:if>
                                        <c:if test="${order.orderStatus==1}">取消</c:if>
                                        <c:if test="${order.orderStatus==2}">入住</c:if>
                                        <c:if test="${order.orderStatus==3}">退房</c:if>
                                    </td>
                                    <td>
                                        <a href="##" onclick="pinglu(kfId=${order.kfId},orderId=${order.id})">评论</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>入住人数：2人</td>
                                    <!--<td><a href="" class="ddxx_dianp" >点评</a></td>-->
                                </tr>
                            </table>
                        </div>
                    </c:forEach>
                    <div id="ddxx_fenye">
                        <a href="${path}/user/dingdan?pageNo=1&typeDingDan=dpl">首页</a>
                        <a href="${path}/user/dingdan?pageNo=${tui.isFirstPage?1:tui.prePage}&typeDingDan=dpl">上一页</a>
                        <c:forEach begin="1" end="${tui.pages}" varStatus="i">
                            <a href="${path}/user/dingdan?pageNow=${i.count}&typeDingDan=dpl">${i.count}</a>
                        </c:forEach>
                        <a href="${path}/user/dingdan?pageNo=${tui.isLastPage?tui.pages:tui.nextPage}&typeDingDan=dpl">下一页</a>
                        <a href="${path}/user/dingdan?pageNo=${tui.pages}&typeDingDan=dpl">尾页</a>
                        <span>${tui.pageNum}/${tui.pages}页</span>
                        <span>共${tui.total}条</span>
                    </div>
                </div>

            </div>

            <!--账户安全-->
            <div id="zhaq">
                <div class="right_title">
                    账户安全
                </div>
                <div class="zhaq_content">
                    <div class="zhaq_content_img">
                        <img src="/img/gougou.png"/>
                    </div>
                    <div class="zhaq_content_grxx">个人信息</div>
                    <span class="zhaq_content_ct">修改个人信息不包括修改密码、手机号、邮箱</span>
                    <span class="zhaq_grxx_xiugai"><a href="/user/updateProyem">修改</a></span>
                </div>
                <div class="zhaq_content">
                    <div class="zhaq_content_img">
                        <img src="/img/gougou.png"/>
                    </div>
                    <div class="zhaq_content_grxx">登录密码</div>
                    <span class="zhaq_content_ct">建议定期更换</span>
                    <span class="zhaq_denglumima"><a href="/user/updateProword">修改</a></span>
                </div>
<%--                <div class="zhaq_content">--%>
<%--                    <div class="zhaq_content_img">--%>
<%--                        <img src="/img/gougou.png"/>--%>
<%--                    </div>--%>
<%--                    <div class="zhaq_content_grxx">绑定邮箱</div>--%>
<%--                    <span class="zhaq_content_ct">绑定邮箱后可以根据邮箱进行找回密码</span>--%>
<%--                    <span class="yx_shouji"><a href="">修改</a></span>--%>
<%--                </div>--%>
<%--                <div class="zhaq_content">--%>
<%--                    <div class="zhaq_content_img">--%>
<%--                        <img src="/img/gougou.png"/>--%>
<%--                    </div>--%>
<%--                    <div class="zhaq_content_grxx">绑定手机</div>--%>
<%--                    <span class="zhaq_content_ct">绑定手机号后可以根据手机号找回密码</span>--%>
<%--                    <span class="yx_shouji"><a href="">修改</a></span>--%>
<%--                </div>--%>
            </div>

        </div>

        <!--页脚-->
        <div id="" style="margin-top: 100px;">
            <img src="/img/yejiao2.png"/>
        </div>
    </div>
</div>
<!-- layer javascript -->
<script src="${path}/js/plugins/layer/layer.min.js"></script>
</body>
<script type="text/javascript">
$(function (){
    var ying= "${sun}";
    if(ying=="1"){
        $(".wdxxx").addClass("liebiaodianji");
        $("#wdxx").show();
        $("#ddxx").hide();
        $("#zhaq").hide();
    }else if(ying=="2"){
        $(".ddxxx").addClass("liebiaodianji");
        $("#wdxx").hide();
        $("#ddxx").show();
        $("#zhaq").hide();
    }

    var typeDingDan= "${typeDingDan}";
    // alert(typeDingDan);
    if(typeDingDan=="qbdd"){
        $(".ddxx_qbdd").addClass("dingdancss");
        $("#quanbudingdan").show();
        $("#jingxingzhong").hide();
        $("#rzhu").hide();
        $("#tui").hide();
    }else if(typeDingDan=="yddd"){
        $(".ddxx_jxz").addClass("dingdancss");
        $("#quanbudingdan").hide();
        $("#jingxingzhong").show();
        $("#rzhu").hide();
        $("#tui").hide();
    }else if(typeDingDan=="rz"){
        $(".ddxx_ycs").addClass("dingdancss");
        $("#quanbudingdan").hide();
        $("#jingxingzhong").hide();
        $("#rzhu").show();
        $("#tui").hide();
    }else if(typeDingDan=="dpl"){
        $(".ddxx_ddp").addClass("dingdancss");
        $("#quanbudingdan").hide();
        $("#jingxingzhong").hide();
        $("#rzhu").hide();
        $("#tui").show();
    }

})
function pinglu(kfId, orderId) {
    // alert(kfId);
    // alert(orderId);
    layer.open({
        type: 2,
        content: '/eva/ping?kfId=' + kfId + '&orderId=' + orderId,
        area: ['800px', '600px']
    })
}
</script>
</html>
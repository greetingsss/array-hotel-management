<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title> 酒店管理系统 - 登录</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<link href="${path}/css/bootstrap.min.css" rel="stylesheet">
	<link href="${path}/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<link href="${path}/css/animate.css" rel="stylesheet">
	<link href="${path}/css/style.css" rel="stylesheet">
	<link href="${path}/css/login.css" rel="stylesheet">
	<!--[if lt IE 9]>
    	<meta http-equiv="refresh" content="0;ie.html" />
    	<![endif]-->
	<script>
        if (window.top !== window.self) {
            window.top.location = window.location;
        }
    </script>
</head>
<body class="signin">
	<div class="signinpanel">
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="/checkLogin">
					<h4 class="no-margins">
						<font color="#F8F8FF">登录</font>
					</h4>
					<p class="m-t-md">
						<font color="#F8F8FF">欢迎登录酒店管理系统</font>
					</p>
					<input type="text" class="form-control uname" placeholder="用户名" name="username" />
					<input type="password" class="form-control pword m-b" placeholder="密码" name="password" />
					<p style="color: red">${msg}</p>
					<button class="btn btn-success btn-block" type="submit">登录</button>
					<a class="btn btn-success btn-block" href="logins.jsp">注册</a>
					<%--<a class="btn btn-success btn-block" href="/loginss.jsp">忘记密码~~</a>--%>
				</form>
			</div>
		</div>
	</div>
</body>

</html>

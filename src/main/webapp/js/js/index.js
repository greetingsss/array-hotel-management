$(function() {
	//设置全部、好评、中评、差评的颜色
	$("#dpsx").children("input").click(function() {
		$(this).parent().children("span").css("color", "black");
		$(this).next().css("color", "#08B2CC");
	})
	/*				$(".dp_rmpx").click(function(){
						$(this).css("color","#08B2CC");
						$(".dp_sjpx").css("color","black");
					})
					$(".dp_sjpx").click(function(){
						$(this).css("color","#08B2CC");
						$(".dp_rmpx").css("color","black");
					})*/

	//设置有用和评论的颜色
	$(".dianzan2").hide();
	$(".xinxi2").hide();
	$(".yhdp_yy").hover(function() {
		$(".dianzan").hide();
		$(".dianzan2").show();
	}, function() {
		$(".dianzan").show();
		$(".dianzan2").hide();
	})
	$(".yhdp_pl").hover(function() {
		$(".xinxi").hide();
		$(".xinxi2").show();
	}, function() {
		$(".xinxi").show();
		$(".xinxi2").hide();
	})

	window.onload = function() {
		var scrollTop = $(this).scrollTop();
		$("#head").addClass("dhlcss");
		if(scrollTop > 0) {
			$("#headTS").show();
		} else if(scrollTop < 0) {
			$("#head").removeClass("dhlcss");
		}
	}

	//设置导航栏
	$(window).scroll(function() {
		var scrollTop = $(this).scrollTop();
		$("#head").addClass("dhlcss");
		if(scrollTop > 0) {
			$("#headTS").show();
		} else if(scrollTop < 0) {
			$("#head").removeClass("dhlcss");
		}
	});

	$(".dh").hover(function() {
		$(this).css("background-color", "white")
		$(this).children().css("color", "#2CABC4")
	}, function() {
		$(this).css("background-color", "#2CABC4")
		$(this).children().css("color", "white")
	})
})
// 基于准备好的dom，初始化echarts实例
// var path=$("#path").val();
// left_0();//迟到
// function left_0(){
//     var myChart_left_0 = echarts.init(document.getElementById('left_0'),'dark');;
//     $.post(path+"/statistics/getCD.do",function (data) {
//         myChart_left_0.setOption({
//             title:{
//                 text:"迟到考勤记录"
//             },
//             xAxis: {
//                 data: ['第一周', '第二周', '第三周', '第四周']
//             },
//             yAxis: {
//                 type: 'value'
//             },
//             series: [{
//                 data: data,
//                 type: 'bar'
//             }]
//         });
//     });
// }
//
// //早退middle_0
// middle_0();
// function middle_0() {
//     var myChart_middle_0 = echarts.init(document.getElementById('middle_0'),'dark');;
//     $.post(path+"/statistics/getZT.do",function (data) {
//         myChart_middle_0.setOption({
//             title:{
//                 text:"早退考勤记录"
//             },
//             xAxis: {
//                 data: ['第一周', '第二周', '第三周', '第四周']
//             },
//             yAxis: {
//                 type: 'value'
//             },
//             series: [{
//                 data: data,
//                 type: 'bar'
//             }]
//         });
//     });
//
//     // var option = {
//     //     title:{
//     //         text:"早退考勤记录"
//     //     },
//     //     xAxis: {
//     //         data: ['第一周', '第二周', '第三周', '第四周']
//     //     },
//     //     yAxis: {
//     //         type: 'value'
//     //     },
//     //     series: [{
//     //         data: [2, 8, 1, 15],
//     //         type: 'bar'
//     //     }]
//     // };
//     // myChart_middle_0.setOption(option);
// }
//
// //缺勤 right_0
// right_0();
// function right_0() {
//     var myChart_right_0 = echarts.init(document.getElementById('right_0'),'dark');
//     $.post(path+"/statistics/getQQ.do",function (data) {
//         myChart_right_0.setOption({
//             title:{
//                 text:"缺勤考勤记录"
//             },
//             xAxis: {
//                 data: ['第一周', '第二周', '第三周', '第四周']
//             },
//             yAxis: {
//                 type: 'value'
//             },
//             series: [{
//                 data: data,
//                 type: 'bar'
//             }]
//         });
//     });
//     // var option = {
//     //     title:{
//     //         text:"缺勤考勤记录"
//     //     },
//     //     xAxis: {
//     //         data: ['第一周', '第二周', '第三周', '第四周']
//     //     },
//     //     yAxis: {
//     //         type: 'value'
//     //     },
//     //     series: [{
//     //         data: [1, 0, 5, 3],
//     //         type: 'bar'
//     //     }]
//     // };
//     // myChart_right_0.setOption(option);
// }
//
// left_1();//迟到早退缺勤占比图
// function left_1(){
//     var myChart_left_1 = echarts.init(document.getElementById('left_1'),'dark');
//
//     $.post(path+"/statistics/getZB.do",function (data) {
//         // alert(data);
//         myChart_left_1.setOption({
//             title: {
//                 text: '考勤记录'
//             },
//             tooltip: {},
//             legend: {
//                 orient: 'vertical',
//                 left: 'left',
//                 data:['迟到','早退','缺勤']
//             },
//             series: [
//                 {
//                     type: 'pie',
//                     data: data
//                 }
//             ]
//         });
//     });
//     // var option = {
//     //     title: {
//     //         text: '考勤记录'
//     //     },
//     //     tooltip: {},
//     //     legend: {
//     //         orient: 'vertical',
//     //         left: 'left',
//     //         data:['迟到','早退','缺勤']
//     //     },
//     //     series: [
//     //         {
//     //             type: 'pie',
//     //             data: [
//     //                 {value: 1048, name: '迟到'},
//     //                 {value: 735, name: '早退'},
//     //                 {value: 580, name: '缺勤'}
//     //             ]
//     //         }
//     //     ]
//     // };
//     //
//     // myChart_left_1.setOption(option);
// }
//
// //迟到早退缺勤
// middle_1();
// function  middle_1() {
//     var myChart_middle_1 = echarts.init(document.getElementById('middle_1'),'dark');
//     var option = {
//         tooltip: {
//             trigger: 'axis',
//             axisPointer: {            // Use axis to trigger tooltip
//                 type: 'shadow'        // 'shadow' as default; can also be 'line' or 'shadow'
//             }
//         },
//         legend: {
//             data: ['迟到', '早退', '缺勤']
//         },
//         grid: {
//             left: '3%',
//             right: '4%',
//             bottom: '3%',
//             containLabel: true
//         },
//         xAxis: {
//             type: 'value'
//         },
//         yAxis: {
//             type: 'category',
//             data: ['第一周', '第二周', '第三周','第四周']
//         },
//         series: [
//             {
//                 name: '迟到',
//                 type: 'bar',
//                 stack: 'total',
//                 label: {
//                     show: true
//                 },
//                 emphasis: {
//                     focus: 'series'
//                 },
//                 data: [320, 302, 301, 334]
//             },
//             {
//                 name: '早退',
//                 type: 'bar',
//                 stack: 'total',
//                 label: {
//                     show: true
//                 },
//                 emphasis: {
//                     focus: 'series'
//                 },
//                 data: [120, 132, 101, 134]
//             },
//             {
//                 name: '缺勤',
//                 type: 'bar',
//                 stack: 'total',
//                 label: {
//                     show: true
//                 },
//                 emphasis: {
//                     focus: 'series'
//                 },
//                 data: [220, 182, 191, 234]
//             }
//         ]
//     };
//     myChart_middle_1.setOption(option);
// }
//
// right_1();
// function right_1(){
//     var myChart_right_1 = echarts.init(document.getElementById('right_1'),'dark');
//     var option = {
//         title: {
//             text: '考勤雷达图'
//         },
//         legend: {
//             data: ['4月考勤', '5月考勤']
//         },
//         radar: {
//             // shape: 'circle',
//             indicator: [
//                 { name: '迟到', max: 150},
//                 { name: '早退', max: 60},
//                 { name: '缺勤', max: 20}
//             ]
//         },
//         series: [{
//             name: '4月考勤 vs 5月考勤',
//             type: 'radar',
//             data: [
//                 {
//                     value: [123, 55, 10],
//                     name: '4月考勤'
//                 },
//                 {
//                     value: [100, 21, 15],
//                     name: '5月考勤'
//                 }
//             ]
//         }]
//     };
//     myChart_right_1.setOption(option);
// }

// aaa();
// function  aaa(){
//     var bbb = echarts.init(document.getElementById('ccc'),'dark');
//     option = {
//         xAxis: {
//             type: 'category',
//             data: ['星期一', '星期二', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
//         },
//         yAxis: {
//             type: 'value'
//         },
//         series: [
//             {
//                 data: [1200, 200, 150, 80, 70, 110, 130],
//                 type: 'bar'
//             }
//         ]
//     };
//     bbb.setOption(option);
// }

aaa();
function  aaa(){
    alert("aaaaaaaa");
    var bbb = echarts.init(document.getElementById('main'),'dark');
    option = {
        title: {
            text: '小雨'
        },
        legend: {
            data: ['Allocated Budget', 'Actual Spending']
        },
        radar: {
            // shape: 'circle',
            indicator: [
                { name: 'Sales', max: 6500 },
                { name: 'Administration', max: 16000 },
                { name: 'Information Technology', max: 30000 },
                { name: 'Customer Support', max: 38000 },
                { name: 'Development', max: 52000 },
                { name: 'Marketing', max: 25000 }
            ]
        },
        series: [
            {
                name: 'Budget vs spending',
                type: 'radar',
                data: [
                    {
                        value: [6500, 3000, 20000, 35000, 50000, 18000],
                        name: 'Allocated Budget'
                    },
                    {
                        value: [6500, 14000, 28000, 26000, 42000, 21000],
                        name: 'Actual Spending'
                    }
                ]
            }
        ]
    };
    bbb.setOption(option);
}




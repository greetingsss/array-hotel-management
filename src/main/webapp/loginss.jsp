
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% String path = request.getContextPath();
	request.setAttribute("path",path);%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<script src="${path}/js/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${path}js/js/login.js" type="text/javascript" charset="utf-8"></script>
		<link rel="${path}stylesheet" type="text/css" href="${path}css/logins.css"/>
	</head>
	<body>
		<div id="login">
			<div id="login_title">
				<span id="jdmc">王府井永光酒店</span>
				<a href="eva/getPage"><img src="img/fangzi.png" width="31px"/><span>首页</span></a>
			</div>
			<div id="img">
				<img src="/img/login001.jpg"/>
				<div id="img_content">
					<form action="/user/selectyong" method="get">
						<table cellpadding="20px" >
							<tr>
								<td colspan="2" id="img_content_zc">
									<span>
										忘记密码~~
									</span>
									<a href="login.jsp">去登录……</a>
								</td>
							</tr>
							<tr>
								<td class="fistTr">用户名：</td>
								<td class="sTr"><input type="text" name="username" /></td>
							</tr>
							<tr>
								<td class="fistTr">身份证号：</td>
								<td class="sTr"><input type="text"  name="idCard"/></td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="submit" value="找回" id="button_zc" />
									<input type="reset" value="清空" id="button_qk" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
			<div id="login_foot">
				<img src="img/yejiao2.png"/>
			</div>
		</div>
	</body>
</html>
